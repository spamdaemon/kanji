#ifndef KANJI_CONJUGATE_H
#define KANJI_CONJUGATE_H

#include <vector>
#include <string>

namespace kanji {

   /**
    * Conjugate a verb.
    * @param utf8 the verb in utf8 form
    * @param ruVerb true if the verb is a RU verb
    * @return an array of columns
    */
   ::std::vector<::std::string> conjugateVerb(const ::std::string& utf8, bool ruVerb);

}

#endif
