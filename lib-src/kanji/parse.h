#ifndef KANJI_PARSE_H
#define KANJI_PARSE_H

#include <iostream>
#include <vector>
#include <string>

namespace kanji {

   /**
    * Read a line of CSV data.
    * @param stream an input stream
    * @return an array of columns
    */
   ::std::vector< ::std::string> readCSVLine(::std::istream& stream);

   /**
    * Read a line of delimited data.
    * @param stream an input stream
    * @param delimiter a delimiter string
    * @return an array of columns
    */
   ::std::vector< ::std::string> readDelimitedLine(::std::istream& stream, const ::std::string& delimiter);

}

#endif
