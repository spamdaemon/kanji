#include <kanji/parse.h>

namespace kanji {

   static void splitLine(const ::std::string& line, const ::std::string& delimiter,
         ::std::vector< ::std::string>& result)
   {
      ::std::string::size_type start = 0;
      ::std::string::size_type end;
      result.clear();
      if (line.empty() || line[0] == '#') {
         return;
      }
      while ((end = line.find(delimiter, start)) != ::std::string::npos) {
         result.push_back(line.substr(start, end - start));
         start = end + delimiter.length();
      }
      result.push_back(line.substr(start, line.length() - start));
   }

   ::std::vector< ::std::string> readCSVLine(::std::istream& stream)
   {
      return readDelimitedLine(stream, ",");
   }

   ::std::vector< ::std::string> readDelimitedLine(::std::istream& stream, const ::std::string& delimiter)
   {
      ::std::vector < ::std::string > result;
      ::std::string line;
      ::std::getline(stream, line);
      if (stream) {
         splitLine(line, delimiter, result);
      }

      return result;
   }

}
