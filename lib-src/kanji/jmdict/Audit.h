#ifndef _KANJI_JMDICT_AUDIT_H
#define _KANJI_JMDICT_AUDIT_H

#include <kanji/jmdict/Text.h>

namespace kanji {
   namespace jmdict {

      struct Audit
      {
            Text upd_date;
            Text upd_detl;
      };
   }
}
#endif
