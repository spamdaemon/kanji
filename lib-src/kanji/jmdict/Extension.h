#ifndef _KANJI_JMDICT_EXTENSION_H
#define _KANJI_JMDICT_EXTENSION_H

#include <kanji/jmdict/Text.h>

namespace kanji {
   namespace jmdict {

      struct Extension
      {
            enum Type
            {
               TEXT, NUMBER
            };

            Text field;
            Type type;
            int number;
            Text text;
      }
      ;
   }
}
#endif
