#ifndef _KANJI_JMDICT_TEXT_H
#define _KANJI_JMDICT_TEXT_H

#include <string>

namespace kanji {
   namespace jmdict {

      /** The UTF8 encoded text */
      typedef ::std::string Text;
   }
}
#endif
