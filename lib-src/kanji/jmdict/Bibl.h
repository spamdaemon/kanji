#ifndef _KANJI_JMDICT_BIBL_H
#define _KANJI_JMDICT_BIBL_H

#include <kanji/jmdict/Text.h>
#include <memory>

namespace kanji {
   namespace jmdict {

      struct Bibl
      {
            Text bib_tag;
            Text bib_txt;
      };
   }
}
#endif
