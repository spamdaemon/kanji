#ifndef _KANJI_JMDICT_SENSE_H
#define _KANJI_JMDICT_SENSE_H

#include <kanji/jmdict/Text.h>
#include <kanji/jmdict/LSource.h>
#include <kanji/jmdict/Gloss.h>
#include <kanji/jmdict/Extension.h>
#include <vector>

namespace kanji {
   namespace jmdict {

      struct Sense
      {
            ::std::vector< Text> stagk;
            ::std::vector< Text> stagr;
            ::std::vector< Text> pos;
            ::std::vector< Text> xref;
            ::std::vector< Text> ant;
            ::std::vector< Text> field;
            ::std::vector< Text> misc;
            ::std::vector< Text> s_inf;
            ::std::vector< LSource> lsource;
            ::std::vector< Text> dial;
            ::std::vector< Gloss> gloss;
            ::std::vector< Text> example;
            ::std::vector< Extension> ext;
      };

      enum class SenseType {
         VERB,
         NOUN,
         ADJECTIVE,
         ADVERB,
         PARTICLE,
         OTHER
      };

      /**
       * Get the sense type.
       * @param sense
       * @return the type of sense
       */
      SenseType typeOf(const Sense& sense);

      /**
       * Determine if this a ru or a u verb or something else.
       * @param sense
       * @return return "r", "u", "-"
       */
      char verbType(const Sense& sense);

      /**
       * Determine if the sense is a transitive or intransitive verb.
       * @param sense
       * @return return true if the sense is a marked as a transitive verb
       */
      bool isTransitiveVerb(const Sense& sense);


      /**
       * Determine if the sense is a transitive or intransitive verb.
       * @param sense
       * @return return true if the sense is a marked as a transitive verb
       */
      bool isIntransitiveVerb(const Sense& sense);

   }
}
#endif
