#ifndef _KANJI_JMDICT_READING_H
#define _KANJI_JMDICT_READING_H

#include <kanji/jmdict/Text.h>
#include <kanji/jmdict/Extension.h>
#include <vector>
#include <memory>

namespace kanji {
   namespace jmdict {

      struct Reading {
            Text reb;
            Text re_nokanji;
            ::std::vector< Text> re_restr;
            ::std::vector< Text> re_inf;
            ::std::vector< Text> re_pri;
            ::std::vector< Extension> ext;
      };
   }
}
#endif
