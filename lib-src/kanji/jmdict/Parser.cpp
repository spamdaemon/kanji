#include <kanji/jmdict/Parser.h>
#include <kanji/jmdict/Entry.h>
#include <timber/w3c/xml/Parser.h>
#include <timber/w3c/xml/DocumentHandler.h>
#include <memory>
#include <vector>
#include <sstream>
#include <iostream>

namespace kanji {
   namespace jmdict {
      namespace {

         struct DocumentHandler : public ::timber::w3c::xml::DocumentHandler
         {
               DocumentHandler(Parser::Callback& cb) throws()
                     : _processEntry(cb)
               {
               }
               ~DocumentHandler() throws()
               {
               }
               void notifyStartElement(const char* name, const char* ns, const char* prefix)
               {
                  const ::std::string element(name);

                  // need to remember the name for the attributes
                  _current = name;
                  if (element == "pri") {
                     _text += "\u00B7";
                  }
                  else {
                     _text.clear();
                  }
                  if (element == "audit") {
                     _audit.reset(new Audit());
                  }
                  else if (element == "bibl") {
                     _bibl.reset(new Bibl());
                  }
                  else if (element == "gloss") {
                     _gloss.reset(new Gloss());
                  }
                  else if (element == "info") {
                     _info.reset(new Info());
                  }
                  else if (element == "k_ele") {
                     _kanji.reset(new Kanji());
                  }
                  else if (element == "r_ele") {
                     _reading.reset(new Reading());
                  }
                  else if (element == "sense") {
                     _sense.reset(new Sense());
                  }
                  else if (element == "entry") {
                     _entry.reset(new Entry());
                  }
                  else if (element == "links") {
                     _link.reset(new Link());
                  }
                  else if (element == "lsource") {
                     _lsource.reset(new LSource());
                  }
               }

               virtual void notifyElementContent()
               {
                  _current.clear();
               }

               virtual void notifyEndElement(const char* name, const char* ns, const char* prefix)
               {
                  ::std::string current = name;
                  ::std::string text = ::std::move(_text);

                  if (current == "entry") {
                     _processEntry(*_entry);
                     _entry = nullptr;
                  }
                  if (current == "ent_seq") {
                     ::std::istringstream s(text);
                     s >> _entry->ent_seq;
                  }
                  if (current == "k_ele") {
                     _entry->kanji.push_back(*_kanji);
                     _kanji = nullptr;
                  }
                  if (current == "keb") {
                     _kanji->keb = text;
                  }
                  if (current == "ke_inf") {
                     _kanji->ke_inf.push_back(text);
                  }
                  if (current == "ke_pri") {
                     _kanji->ke_pri.push_back(text);
                  }

                  if (current == "r_ele") {
                     _entry->reading.push_back(*_reading);
                     _reading = nullptr;
                  }
                  if (current == "reb") {
                     _reading->reb = text;
                  }
                  if (current == "re_nokanji") {
                     _reading->re_nokanji = text;
                  }
                  if (current == "re_restr") {
                     _reading->re_restr.push_back(text);
                  }
                  if (current == "re_inf") {
                     _reading->re_inf.push_back(text);
                  }
                  if (current == "re_pri") {
                     _reading->re_pri.push_back(text);
                  }
                  if (current == "info") {
                     _entry->info = ::std::move(_info);
                     _info = nullptr;
                  }
                  if (current == "bibl") {
                     _info->bibl.push_back(*_bibl);
                     _bibl = nullptr;
                  }
                  if (current == "bib_tag") {
                     _bibl->bib_tag = text;
                  }
                  if (current == "bib_txt") {
                     _bibl->bib_txt = text;
                  }
                  if (current == "etym") {
                     _info->etym.push_back(text);
                  }
                  if (current == "links") {
                     _info->links.push_back(*_link);
                     _link = nullptr;
                  }
                  if (current == "link_tag") {
                     _link->link_tag = text;
                  }
                  if (current == "link_desc") {
                     _link->link_desc = text;
                  }
                  if (current == "link_uri") {
                     _link->link_uri = text;
                  }
                  if (current == "audit") {
                     _info->audit.push_back(*_audit);
                     _audit = nullptr;
                  }
                  if (current == "upd_date") {
                     _audit->upd_date = text;
                  }
                  if (current == "upd_detl") {
                     _audit->upd_detl = text;
                  }
                  if (current == "sense") {
                     if (_sense->pos.empty() && !_entry->sense.empty()) {
                        _sense->pos = _entry->sense.back().pos;
                     }
                     _entry->sense.push_back(*_sense);
                     _sense = nullptr;
                  }
                  if (current == "stagk") {
                     _sense->stagk.push_back(text);
                  }
                  if (current == "stagr") {
                     _sense->stagr.push_back(text);
                  }
                  if (current == "xref") {
                     _sense->xref.push_back(text);
                  }
                  if (current == "ant") {
                     _sense->ant.push_back(text);
                  }
                  if (current == "pos") {
                     _sense->pos.push_back(text);
                  }
                  if (current == "field") {
                     _sense->field.push_back(text);
                  }
                  if (current == "misc") {
                     _sense->misc.push_back(text);
                  }
                  if (current == "lsource") {
                     _sense->lsource.push_back(*_lsource);
                     _lsource = nullptr;
                  }
                  if (current == "dial") {
                     _sense->dial.push_back(text);
                  }
                  if (current == "gloss") {
                     _gloss->gloss = text;
                     _sense->gloss.push_back(*_gloss);
                     _gloss = nullptr;
                  }
                  if (current == "pri") {
                     // keep the pri text
                     _text = ::std::move(text);
                     _text += "\u00B7";
                  }
                  if (current == "example") {
                     _sense->example.push_back(text);
                  }
                  if (current == "s_inf") {
                     _sense->s_inf.push_back(text);
                  }
               }

               void notifyCharacterData(const char* txt, int count)
               {
                  _text.append(txt, count);
               }

               void notifyAttribute(const char* name, const char* ns, const char* prefix, const char* value)
               {
                  ::std::string attr(name);
                  if (_current == "gloss") {
                     if (attr == "g_gend") {
                        _gloss->g_gend = value;
                     }
                     else if (attr == "lang") {
                        _gloss->lang = value;
                     }
                  }
                  else if (_current == "lsource") {
                     if (attr == "ls_type") {
                        _lsource->ls_type = value;
                     }
                     if (attr == "ls_wasei") {
                        _lsource->ls_wasei = value;
                     }
                     else if (attr == "lang") {
                        _lsource->lang = value;
                     }
                  }
               }

               /** The current element */
            private:
               ::std::string _current;
               /** A text buffer */
            private:
               ::std::string _text;

               // the element stack
               ::std::vector< ::std::string> _stack;

               /** The current entry being processed */
               ::std::shared_ptr< Entry> _entry;

               /** The current object being processed */
               ::std::unique_ptr< Audit> _audit;
               ::std::unique_ptr< Bibl> _bibl;
               ::std::unique_ptr< Gloss> _gloss;
               ::std::unique_ptr< Info> _info;
               ::std::unique_ptr< Kanji> _kanji;
               ::std::unique_ptr< Link> _link;
               ::std::unique_ptr< LSource> _lsource;
               ::std::unique_ptr< Reading> _reading;
               ::std::unique_ptr< Sense> _sense;
               Text _pri;

               /** The entry */
               Parser::Callback& _processEntry;
         };
      }
      Parser::Parser() throw()
      {
      }
      Parser::~Parser() throw()
      {
      }
      void Parser::parseXml(::std::istream& stream, Callback cb) const
      {
         DocumentHandler handler(cb);
         auto parser = ::timber::w3c::xml::Parser::create();
         parser->parseStream(stream, handler);
      }

   }
}
