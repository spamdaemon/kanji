#ifndef _KANJI_JMDICT_LSOURCE_H
#define _KANJI_JMDICT_LSOURCE_H

#include <kanji/jmdict/Text.h>
#include <kanji/jmdict/Language.h>

namespace kanji {
   namespace jmdict {

      struct LSource
      {
            Text lsource;
            Language lang;
            Text ls_type;
            Text ls_wasei;
      };
   }
}
#endif
