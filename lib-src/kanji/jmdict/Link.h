#ifndef _KANJI_JMDICT_LINK_H
#define _KANJI_JMDICT_LINK_H

#include <kanji/jmdict/Text.h>

namespace kanji {
   namespace jmdict {

      struct Link
      {
            Text link_tag;
            Text link_desc;
            Text link_uri;
      };
   }
}
#endif
