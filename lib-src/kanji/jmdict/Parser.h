#ifndef KANJI_JMDICT_PARSER_H
#define KANJI_JMDICT_PARSER_H

#include <functional>
#include <istream>
#include <string>

#include "Entry.h"

namespace kanji {
   namespace jmdict {

      /** The entry returned in the callback */
      class Entry;

      class Parser
      {
         public:
            typedef ::std::function< bool(const Entry&)> Callback;

            /**
             * Create a new JMDict parser.
             */
         public:
            Parser() throw();

            /**
             * Destructor
             */
         public:
            ~Parser() throw();

            /**
             * Parse a JMDict file available on the specified stream.
             * @param stream a stream
             * @param cb a callback.
             * @throw ::std::exception on error
             */
         public:
            void parseXml(::std::istream& stream, Callback cb) const;
      };
   }
}
#endif
