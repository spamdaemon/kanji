#ifndef _KANJI_JMDICT_KANJI_H
#define _KANJI_JMDICT_KANJI_H

#include <kanji/jmdict/Text.h>
#include <kanji/jmdict/Extension.h>
#include <vector>

namespace kanji {
   namespace jmdict {

      struct Kanji
      {
            Text keb;
            ::std::vector< Text> ke_inf;
            ::std::vector< Text> ke_pri;
            ::std::vector< Extension> ext;
      };
   }
}
#endif
