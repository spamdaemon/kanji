#ifndef _KANJI_JMDICT_LANGUAGE_H
#define _KANJI_JMDICT_LANGUAGE_H

#include <string>

namespace kanji {
   namespace jmdict {

      /** The UTF8 encoded text */
      typedef ::std::string Language;
   }
}
#endif
