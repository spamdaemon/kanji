#ifndef KANJI_JMDICT_PART_OF_SPEECH_H
#define KANJI_JMDICT_PART_OF_SPEECH_H

#ifndef _KANJI_JMDICT_TEXT_H
#include <kanji/jmdict/Text.h>
#endif

namespace kanji {
   namespace jmdict {

      /**
       * This is the reverse map for the part of speech.
       * @param pos the part of speech in JMDict
       * @return return name of the corresponding entity in JMDIct
       */
      const char* map_pos(const Text& pos);

      bool isProperWord(const Text& pos);
   }
}
#endif
