#include <kanji/jmdict/Sense.h>
#include <kanji/jmdict/Text.h>
#include <kanji/jmdict/PartOfSpeech.h>

namespace kanji {
   namespace jmdict {

      SenseType typeOf(const Sense& sense)
      {
         SenseType res = SenseType::OTHER;
         for (auto& xpos : sense.pos) {
            const char* key = map_pos(xpos);
            if (key == nullptr) {
               continue;
            }
            const Text pos(key);

            if (pos == "adj" || pos.find("adj-") == 0) {
               res = SenseType::ADJECTIVE;
            }
            else if (pos == "n" || pos.find("n-") == 0) {
               res = SenseType::NOUN;
            }
            else if (pos == "prt") {
               res = SenseType::PARTICLE;
            }
            else if (pos[0] == 'v') {
               if (res == SenseType::OTHER) {
                  res = SenseType::VERB;
               }
            }

         }
         return res;
      }

      char verbType(const Sense& sense)
      {
         for (auto& pos : sense.pos) {
            if (pos.find("Ichidan verb") == 0) {
               return 'r';
            }
            if (pos.find("Godan verb") == 0) {
               return 'u';
            }
         }
         return '-';
      }

      bool isTransitiveVerb(const Sense& sense)
      {
         for (auto& pos : sense.pos) {
            if (pos.find("intransitive verb") == 0) {
               return false;
            }
            if (pos.find("transitive verb") == 0) {
               return true;
            }
         }
         return false;
      }
      bool isIntransitiveVerb(const Sense& sense)
      {
         for (auto& pos : sense.pos) {
            if (pos.find("intransitive verb") == 0) {
               return true;
            }
            if (pos.find("transitive verb") == 0) {
               return false;
            }
         }
         return false;
      }
   }
}
