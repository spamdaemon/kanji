#ifndef _KANJI_JMDICT_GLOSS_H
#define _KANJI_JMDICT_GLOSS_H

#include <kanji/jmdict/Text.h>
#include <kanji/jmdict/Language.h>
#include <kanji/jmdict/Extension.h>
#include <vector>

namespace kanji {
   namespace jmdict {

      struct Gloss
      {
            Text gloss;
            Text pri;
            Language lang;
            Text g_gend;
            ::std::vector< Extension> ext;
      };
   }
}
#endif
