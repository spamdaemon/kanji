#include <kanji/jmdict/Entry.h>
#include <timber/logging.h>
#include <canopy/ByteOrder.h>
#include <iostream>
#include <sstream>
#include <fstream>
#include <memory>

namespace kanji {
   namespace jmdict {
      namespace {

         static unsigned int read(char const*& ptr, char const* end)
         {
            if (ptr == end) {
               ::std::abort();
               throw ::std::runtime_error("Reading beyond array");
            }
            return static_cast< unsigned int>(*(ptr++) & 0x0ff);
         }

         static void write(char *& ptr, char const* end, int value)
         {
            if (ptr == end) {
               ::std::abort();
               throw ::std::runtime_error("Writing beyond array");
            }
            *(ptr++) = static_cast< char>(value & 0xff);
         }

         static unsigned short readUShort(char const*& ptr, char const* end)
         {
            auto c = read(ptr, end);
            auto d = read(ptr, end);
            return (c << 8) | (d << 0);
         }

         static unsigned int readUInt(char const*& ptr, char const* end)
         {
            auto a = read(ptr, end);
            auto b = read(ptr, end);
            auto c = read(ptr, end);
            auto d = read(ptr, end);
            return (a << 24) | (b << 16) | (c << 8) | (d << 0);
         }
         static int readInt(char const*& ptr, char const* end)
         {
            auto a = read(ptr, end);
            auto b = read(ptr, end);
            auto c = read(ptr, end);
            auto d = read(ptr, end);
            return (a << 24) | (b << 16) | (c << 8) | (d << 0);
         }

         static unsigned short readUShort(::std::istream& in)
         {
            int c = in.get();
            int d = in.get();
            if (!in) {
               return 0;
            }
            return ((c & 0x0ff) << 8) | ((d & 0x0ff) << 0);
         }

         inline static void writeUShort(char*& ptr, char const* end, unsigned short host)
         {
            write(ptr, end, host >> 8);
            write(ptr, end, host);
         }

         inline static void writeShort(::std::ostream& out, unsigned short host)
         {
            auto network = ::canopy::toNetworkByteOrder(host);
            out.write(reinterpret_cast< char*>(&network), sizeof(network));
         }
         inline static void writeInt(::std::ostream& out, unsigned int host)
         {
            auto network = ::canopy::toNetworkByteOrder(host);
            out.write(reinterpret_cast< char*>(&network), sizeof(network));
         }
         inline static void writeSignedInt(::std::ostream& out, int host)
         {
            auto network = ::canopy::toNetworkByteOrder(host);
            out.write(reinterpret_cast< char*>(&network), sizeof(network));
         }
         inline static void writeShort(::std::ostream& out, size_t host)
         {
            writeShort(out, static_cast< unsigned short>(host));
         }

         static size_t writeText(::std::ostream& out, const Text& text)
         {
            size_t n = text.size();
            out.write(text.c_str(), text.size());
            out.write("\0", 1);
            return n + 1;
         }

         static size_t writeLanguage(::std::ostream& out, const Language& text)
         {
            size_t n = text.size();
            out.write(text.c_str(), text.size());
            out.write("\0", 1);
            return n + 1;
         }
         static size_t write(::std::ostream& obj, size_t offset, ::std::ostream& strings, const Text& text)
         {
            // write the index into the string buffer
            writeShort(obj, offset);
            // write the string itself
            return offset + writeText(strings, text);
         }
         static void read(char const*& pos, char const* end, char const* strings, char const* send, Text& text)
         {
            size_t offset = readUShort(pos, end);
            char const* textPtr = strings + offset;
            if (textPtr >= send) {
               ::std::abort();
               throw ::std::runtime_error("Invalid string index");
            }
            for (char const* s = textPtr; s < send; ++s) {
               if (*s == '\0') {
                  text = Text(textPtr);
                  return;
               }
            }
            ::std::abort();
            throw ::std::runtime_error("Invalid string found");
         }

         static size_t writeLanguage(::std::ostream& obj, size_t offset, ::std::ostream& strings, const Language& text)
         {
            // write the index into the string buffer
            writeShort(obj, offset);
            // write the string itself
            return offset + writeLanguage(strings, text);
         }

         static void read(char const*& pos, char const* end, char const* strings, char const* send,
               ::std::vector< Text>& text)
         {
            text.resize(readUShort(pos, end));
            for (Text& t : text) {
               read(pos, end, strings, send, t);
            }
         }

         static void readExtensions(char const*& pos, char const* end, char const* strings, char const* send,
               ::std::vector< Extension>& ext)
         {
            ext.resize(readUShort(pos, end));
            for (Extension& obj : ext) {
               read(pos, end, strings, send, obj.field);
               switch (readInt(pos, end)) {
                  case Extension::TEXT:
                     obj.type = Extension::TEXT;
                     read(pos, end, strings, send, obj.text);
                     break;
                  case Extension::NUMBER:
                     obj.type = Extension::NUMBER;
                     obj.number = readInt(pos, end);
                     break;
                  default:
                     ::std::abort();
                     throw ::std::runtime_error("Unsupported extension type");
               }
            }
         }

         static size_t writeExtensions(::std::ostream& obj, size_t offset, ::std::ostream& strings,
               const ::std::vector< Extension>& exts)
         {
            writeShort(obj, exts.size());
            for (const Extension& ext : exts) {
               offset = write(obj, offset, strings, ext.field);
               writeInt(obj, (int) ext.type);
               switch (ext.type) {
                  case Extension::TEXT:
                     offset = write(obj, offset, strings, ext.text);
                     break;
                  case Extension::NUMBER:
                     writeSignedInt(obj, ext.number);
                     break;
                  default:
                     ::std::abort();
                     timber::logging::Log("kanji.jmdict.Entry.writeExtensions").warn("Unknown extension encountered");
                     break;
               }
            }
            return offset;
         }

         static size_t write(::std::ostream& obj, size_t offset, ::std::ostream& strings,
               const ::std::vector< Text>& text)
         {
            writeShort(obj, text.size());
            for (const Text& t : text) {
               offset = write(obj, offset, strings, t);
            }
            return offset;
         }
      }

      ::std::ostream& operator<<(::std::ostream& out, const Entry& e)
      {
         ::std::ostringstream strings, entryBuf;

         // the first string position is 0
         unsigned short stringPos = 0;

         writeInt(entryBuf, e.ent_seq);
         writeShort(entryBuf, e.kanji.size());
         for (Kanji k : e.kanji) {
            stringPos = write(entryBuf, stringPos, strings, k.keb);
            stringPos = write(entryBuf, stringPos, strings, k.ke_inf);
            stringPos = write(entryBuf, stringPos, strings, k.ke_pri);
            stringPos = writeExtensions(entryBuf, stringPos, strings, k.ext);
         }
         writeShort(entryBuf, e.reading.size());
         for (Reading r : e.reading) {
            stringPos = write(entryBuf, stringPos, strings, r.reb);
            stringPos = write(entryBuf, stringPos, strings, r.re_nokanji);
            stringPos = write(entryBuf, stringPos, strings, r.re_restr);
            stringPos = write(entryBuf, stringPos, strings, r.re_inf);
            stringPos = write(entryBuf, stringPos, strings, r.re_pri);
            stringPos = writeExtensions(entryBuf, stringPos, strings, r.ext);
         }
         writeShort(entryBuf, e.sense.size());
         for (Sense s : e.sense) {
            writeShort(entryBuf, s.gloss.size());
            for (Gloss g : s.gloss) {
               stringPos = write(entryBuf, stringPos, strings, g.gloss);
               stringPos = writeLanguage(entryBuf, stringPos, strings, g.lang);
               stringPos = write(entryBuf, stringPos, strings, g.pri);
               stringPos = write(entryBuf, stringPos, strings, g.g_gend);
               stringPos = writeExtensions(entryBuf, stringPos, strings, g.ext);
            }
            stringPos = write(entryBuf, stringPos, strings, s.example);
            stringPos = write(entryBuf, stringPos, strings, s.ant);
            stringPos = write(entryBuf, stringPos, strings, s.dial);
            stringPos = write(entryBuf, stringPos, strings, s.field);
            writeShort(entryBuf, s.lsource.size());
            for (LSource ls : s.lsource) {
               stringPos = write(entryBuf, stringPos, strings, ls.lsource);
               stringPos = writeLanguage(entryBuf, stringPos, strings, ls.lang);
               stringPos = write(entryBuf, stringPos, strings, ls.ls_type);
               stringPos = write(entryBuf, stringPos, strings, ls.ls_wasei);
            }

            stringPos = write(entryBuf, stringPos, strings, s.misc);
            stringPos = write(entryBuf, stringPos, strings, s.pos);
            stringPos = write(entryBuf, stringPos, strings, s.xref);
            stringPos = write(entryBuf, stringPos, strings, s.s_inf);
            stringPos = write(entryBuf, stringPos, strings, s.stagk);
            stringPos = write(entryBuf, stringPos, strings, s.stagr);
            stringPos = writeExtensions(entryBuf, stringPos, strings, s.ext);
         }
         if (!e.info) {
            writeShort(entryBuf, 0x0UL);
         }
         else {
            writeShort(entryBuf, 1UL);
            writeShort(entryBuf, e.info->audit.size());
            for (Audit a : e.info->audit) {
               stringPos = write(entryBuf, stringPos, strings, a.upd_date);
               stringPos = write(entryBuf, stringPos, strings, a.upd_detl);
            }
            writeShort(entryBuf, e.info->bibl.size());
            for (Bibl b : e.info->bibl) {
               stringPos = write(entryBuf, stringPos, strings, b.bib_tag);
               stringPos = write(entryBuf, stringPos, strings, b.bib_txt);
            }
            writeShort(entryBuf, e.info->links.size());
            for (Link l : e.info->links) {
               stringPos = write(entryBuf, stringPos, strings, l.link_uri);
               stringPos = write(entryBuf, stringPos, strings, l.link_tag);
               stringPos = write(entryBuf, stringPos, strings, l.link_desc);
            }
            stringPos = write(entryBuf, stringPos, strings, e.info->etym);
            stringPos = writeExtensions(entryBuf, stringPos, strings, e.info->ext);
         }
         stringPos = writeExtensions(entryBuf, stringPos, strings, e.ext);

         // write out the string table offset
         const ::std::string stringTable = strings.str();
         const ::std::string entryData = entryBuf.str();

         writeShort(out, stringTable.size());
         writeShort(out, entryData.size());
         out.write(stringTable.c_str(), stringTable.size());
         out.write(entryData.c_str(), entryData.size());
         return out;
      }

      RawEntry readRawEntry(::std::istream& in)
      {
         size_t stringSize = readUShort(in);
         if (!in) {
            return {0, nullptr};
         }
         size_t entrySize = readUShort(in);
         if (!in) {
            return {0, nullptr};
         }
         size_t totalSize = stringSize + entrySize;
         // read the string table
         size_t offset = 2 * sizeof(unsigned short);
         ::std::unique_ptr< char[]> buf(new char[offset + totalSize]);
         in.read(buf.get() + offset, totalSize);
         if (!in || in.gcount() != totalSize) {
            in.clear(::std::ios::failbit);
            return {0, nullptr};
         }
         char* pos = buf.get();
         char const* end = pos + totalSize;
         writeUShort(pos, end, stringSize);
         writeUShort(pos, end, entrySize);
         return {totalSize+offset, ::std::move(buf)};
      }

      ::std::istream& operator>>(::std::istream& in, Entry& e)
      {
         e.ent_seq = 0;
         e.kanji.clear();
         e.reading.clear();
         e.sense.clear();
         e.info.reset();
         e.ext.clear();

         size_t stringSize = readUShort(in);
         if (!in) {
            return in;
         }
         size_t entrySize = readUShort(in);
         if (!in) {
            return in;
         }
         size_t totalSize = stringSize + entrySize;
         // read the string table
         ::std::unique_ptr< char[]> strings(new char[totalSize]);
         in.read(strings.get(), totalSize);
         if (!in || in.gcount() != totalSize) {
            in.clear(::std::ios::failbit);
            return in;
         }

         char const* sstart = strings.get();
         char const* send = strings.get() + stringSize;
         char const* pos = strings.get() + stringSize;
         char const* end = strings.get() + totalSize;

         try {
            e.ent_seq = readUInt(pos, end);
            e.kanji.resize(readUShort(pos, end));
            for (Kanji& obj : e.kanji) {
               read(pos, end, sstart, send, obj.keb);
               read(pos, end, sstart, send, obj.ke_inf);
               read(pos, end, sstart, send, obj.ke_pri);
               readExtensions(pos, end, sstart, send, obj.ext);
            }
            e.reading.resize(readUShort(pos, end));
            for (Reading& obj : e.reading) {
               read(pos, end, sstart, send, obj.reb);
               read(pos, end, sstart, send, obj.re_nokanji);
               read(pos, end, sstart, send, obj.re_restr);
               read(pos, end, sstart, send, obj.re_inf);
               read(pos, end, sstart, send, obj.re_pri);
               readExtensions(pos, end, sstart, send, obj.ext);
            }
            e.sense.resize(readUShort(pos, end));
            for (Sense& obj : e.sense) {
               obj.gloss.resize(readUShort(pos, end));
               for (Gloss& gloss : obj.gloss) {
                  read(pos, end, sstart, send, gloss.gloss);
                  read(pos, end, sstart, send, gloss.lang);
                  read(pos, end, sstart, send, gloss.pri);
                  read(pos, end, sstart, send, gloss.g_gend);
                  readExtensions(pos, end, sstart, send, gloss.ext);
               }
               read(pos, end, sstart, send, obj.example);
               read(pos, end, sstart, send, obj.ant);
               read(pos, end, sstart, send, obj.dial);
               read(pos, end, sstart, send, obj.field);
               obj.lsource.resize(readUShort(pos, end));
               for (LSource& ls : obj.lsource) {
                  read(pos, end, sstart, send, ls.lsource);
                  read(pos, end, sstart, send, ls.lang);
                  read(pos, end, sstart, send, ls.ls_type);
                  read(pos, end, sstart, send, ls.ls_wasei);
               }
               read(pos, end, sstart, send, obj.misc);
               read(pos, end, sstart, send, obj.pos);
               read(pos, end, sstart, send, obj.xref);
               read(pos, end, sstart, send, obj.s_inf);
               read(pos, end, sstart, send, obj.stagk);
               read(pos, end, sstart, send, obj.stagr);
               readExtensions(pos, end, sstart, send, obj.ext);
            }
            unsigned short haveInfo = readUShort(pos, end);
            if (haveInfo == 1) {
               e.info.reset(new Info());
               e.info->audit.resize(readUShort(pos, end));
               for (Audit& obj : e.info->audit) {
                  read(pos, end, sstart, send, obj.upd_date);
                  read(pos, end, sstart, send, obj.upd_detl);
               }
               e.info->bibl.resize(readUShort(pos, end));
               for (Bibl& obj : e.info->bibl) {
                  read(pos, end, sstart, send, obj.bib_tag);
                  read(pos, end, sstart, send, obj.bib_txt);
               }
               e.info->links.resize(readUShort(pos, end));
               for (Link& obj : e.info->links) {
                  read(pos, end, sstart, send, obj.link_uri);
                  read(pos, end, sstart, send, obj.link_tag);
                  read(pos, end, sstart, send, obj.link_desc);
               }
               read(pos, end, sstart, send, e.info->etym);
               readExtensions(pos, end, sstart, send, e.info->ext);
            }
            readExtensions(pos, end, sstart, send, e.ext);
         }
         catch (...) {
            in.clear(::std::ios::failbit);
         }
         return in;
      }

      void readEntries(::std::istream& stream, ::std::function< void(Entry&)> f)
      {
         kanji::jmdict::Entry e;
         while (stream) {
            stream >> e;
            if (stream.fail()) {
               if (!stream.eof()) {
                  ::std::cerr << "Failed to read entry" << ::std::endl;
               }
               break;
            }
            else {
               f(e);
            }
         }
      }

      void readEntries(const char* source, ::std::function< void(Entry&)> f)
      {
         if (source == nullptr) {
            ::std::cerr << "No file specified; reading from stdin" << ::std::endl;
            readEntries(::std::cin, f);
         }
         else {
            ::std::ifstream stream(source);
            readEntries(stream, f);
         }
      }

   }
}
