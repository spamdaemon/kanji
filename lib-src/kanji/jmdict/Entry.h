#ifndef _KANJI_JMDICT_ENTRY_H
#define _KANJI_JMDICT_ENTRY_H

#include <kanji/jmdict/Kanji.h>
#include <kanji/jmdict/Reading.h>
#include <kanji/jmdict/Info.h>
#include <kanji/jmdict/Sense.h>
#include <kanji/jmdict/Extension.h>

#include <vector>
#include <memory>
#include <ostream>
#include <functional>

namespace kanji {
   namespace jmdict {

      struct Entry
      {

         public:
            typedef unsigned int SequenceNumber;

            /** The sequence number for this entry */
            SequenceNumber ent_seq;

            /** The kanji elements */
            ::std::vector< Kanji> kanji;

            /** The reading elements */
            ::std::vector< Reading> reading;

            /** Information about the entry */
            ::std::shared_ptr< Info> info;

            /** The senses */
            ::std::vector< Sense> sense;

            /** Any extensions */
            ::std::vector< Extension> ext;
      };

      struct RawEntry {
            size_t n;
            ::std::unique_ptr< char[]> buffer;
      };

      void readEntries(const char* source, ::std::function<void(Entry&)> f);
      void readEntries(::std::istream& stream, ::std::function<void(Entry&)> f);

      RawEntry readRawEntry(::std::istream& in);
      ::std::ostream& operator<<(::std::ostream& out, const Entry& e);
      ::std::istream& operator>>(::std::istream& in, Entry& e);
   }
}
#endif
