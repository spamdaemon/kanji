#ifndef _KANJI_JMDICT_INFO_H
#define _KANJI_JMDICT_INFO_H

#include <kanji/jmdict/Link.h>
#include <kanji/jmdict/Bibl.h>
#include <kanji/jmdict/Audit.h>
#include <kanji/jmdict/Text.h>
#include <kanji/jmdict/Extension.h>
#include <vector>

namespace kanji {
   namespace jmdict {

      struct Info
      {
            ::std::vector<Link> links;
            ::std::vector<Bibl> bibl;
            ::std::vector<Text> etym;
            ::std::vector<Audit> audit;
            ::std::vector< Extension> ext;
      };
   }
}
#endif
