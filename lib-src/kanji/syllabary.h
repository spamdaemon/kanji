#ifndef _KANJI_SYLLABARY_H
#define _KANJI_SYLLABARY_H

namespace kanji {

   /** The hiragana characters (utf8 strings) */
   extern const char* HIRAGANA[];

   /** The katakana characters (utf8 strings) */
   extern const char* KATAKANA[];

}

#endif
