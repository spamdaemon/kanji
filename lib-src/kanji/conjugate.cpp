#include <timber/text/unicode/UnicodeString.h>
#include <string>

namespace kanji {

   using namespace ::timber::text::unicode;

   enum class Type
   {
      KU, GU, SU, BU, MU, NU, RU, TSU, U, RU_RU // special type RU verb
   };

   static UnicodeString pop(const UnicodeString& verb, size_t count = 1)
   {
      const size_t n = verb.length();
      return verb.substring(0, n - count);
   }

   static UnicodeString join(const UnicodeString& s, const char* suffix)
   {
      return s + ::std::string(suffix);
   }

   static UnicodeString joinpop(const UnicodeString& verb, size_t count, const char* suffix)
   {
      return join(pop(verb, count), suffix);
   }

   static UnicodeString join(const UnicodeString& s, const ::std::string& suffix)
   {
      return s + suffix;
   }

   static UnicodeString join(const UnicodeString& s, const UnicodeString& suffix)
   {
      return s + suffix;
   }

   static UnicodeString stem(const UnicodeString& verb)
   {
      return pop(verb);
   }

   static UnicodeString tail(const UnicodeString& verb)
   {
      const size_t n = verb.length();
      return verb.substring(n - 1, 1);
   }

   static UnicodeString uc(const char* str)
   {
      return UnicodeString(::std::string(str));
   }

   static Type type(const UnicodeString& verb, bool ruVerb)
   {
      auto x = tail(verb);
      if (x == uc("く")) {
         return Type::KU;
      }
      else if (x == uc("ぐ")) {
         return Type::GU;
      }
      else if (x == uc("す")) {
         return Type::SU;
      }
      else if (x == uc("ぶ")) {
         return Type::BU;
      }
      else if (x == uc("む")) {
         return Type::MU;
      }
      else if (x == uc("ぬ")) {
         return Type::NU;
      }
      else if (x == uc("る")) {
         if (ruVerb) {
            return Type::RU_RU;
         }
         else {
            return Type::RU;
         }
      }
      else if (x == uc("つ")) {
         return Type::TSU;
      }
      else if (x == uc("う")) {
         return Type::U;
      }
      throw new ::std::runtime_error("Invalid verb extension " + x);
   }

#if 0
   switch(type(verb,ruVerb)) {
      case Type::BU:
      break;
      break; case Type::GU:
      break; case Type::KU:
      case Type::MU:
      break; case Type::NU:
      case Type::RU:
      break;case Type::RU_RU:
      break; case Type::SU:
      break; case Type::TSU:
      break; case Type::U:
      break;}
#endif

   static UnicodeString simple_present_positive(const UnicodeString& verb, bool)
   {
      return verb;
   }

   static UnicodeString simple_present_negative(const UnicodeString& verb, bool ruVerb)
   {
      if (verb == uc("する")) {
         return uc("しない");
      }
      if (verb == uc("くる") || verb == uc("来る")) {
         return uc("こない");
      }
      if (verb == uc("ある")) {
         return uc("ない");
      }

      ::std::string ext;
      switch (type(verb, ruVerb)) {
         case Type::BU:
            ext = "ばない";
            break;
         case Type::GU:
            ext = "がない";
            break;
         case Type::KU:
            ext = "かない";
            break;
         case Type::MU:
            ext = "まない";
            break;
         case Type::NU:
            ext = "なない";
            break;
         case Type::RU:
            ext = "らない";
            break;
         case Type::RU_RU:
            ext = "ない";
            break;
         case Type::SU:
            ext = "さない";
            break;
         case Type::TSU:
            ext = "たない";
            break;
         case Type::U:
            ext = "わない";
            break;
      }
      return join(stem(verb), ext);
   }

   static UnicodeString simple_past_positive(const UnicodeString& verb, bool ruVerb)
   {
      if (verb == uc("する")) {
         return uc("した");
      }
      if (verb == uc("くる") || verb == uc("来る")) {
         return uc("きた");
      }
      if (verb == uc("行く")) {
         return uc("行った");
      }

      UnicodeString ext;
      switch (type(verb, ruVerb)) {
         case Type::KU:
            ext = "いた";
            break;
         case Type::GU:
            ext = "いだ";
            break;
         case Type::BU:
         case Type::MU:
         case Type::NU:
            ext = "んだ";
            break;
         case Type::RU:
         case Type::U:
         case Type::TSU:
            ext = "った";
            break;
         case Type::RU_RU:
            ext = "た";
            break;
         case Type::SU:
            ext = "した";
            break;
      }

      return join(stem(verb), ext);
   }

   static UnicodeString simple_past_negative(const UnicodeString& verb, bool ruVerb)
   {
      return join(pop(simple_present_negative(verb, ruVerb)), "かった");
   }

   static UnicodeString imperative_form(const UnicodeString& verb, bool ruVerb)
   {
      if (verb == uc("する")) {
         return uc("しろ");
      }
      if (verb == uc("くる") || verb == uc("来る")) {
         return uc("こい");
      }
      if (verb == uc("おっしゃる")) {
         return uc("おっしゃい");
      }
      ::std::string ext;
      switch (type(verb, ruVerb)) {
         case Type::BU:
            ext = "べ";
            break;
         case Type::GU:
            ext = "げ";
            break;
         case Type::KU:
            ext = "け";
            break;
         case Type::MU:
            ext = "め";
            break;
         case Type::NU:
            ext = "ね";
            break;
         case Type::RU:
            ext = "れ";
            break;
         case Type::RU_RU:
            ext = "ろ";
            break;
         case Type::SU:
            ext = "せ";
            break;
         case Type::TSU:
            ext = "て";
            break;
         case Type::U:
            ext = "え";
            break;
      }
      return join(stem(verb), ext);
   }



   static UnicodeString potential_form(const UnicodeString& verb, bool ruVerb)
   {
      if (verb == uc("ある")) {
         return uc("");
      }
      if (verb == uc("する")) {
         return uc("できる");
      }
      if (verb == uc("くる") || verb == uc("来る")) {
         return uc("こられる");
      }
      if (verb == uc("おっしゃる")) {
         return uc("おっしゃれる");
      }

      if (type(verb,ruVerb)==Type::RU_RU) {
         return joinpop(imperative_form(verb,ruVerb),1,"られる");
      }
      else {
         return join(imperative_form(verb,ruVerb), "る");
      }
   }
   static UnicodeString conditional_form(const UnicodeString& verb, bool ruVerb)
   {
      if (verb == uc("する")) {
         return uc("すれば");
      }
      if (verb == uc("くる") || verb == uc("来る")) {
         return uc("くれば");
      }
      if (verb == uc("おっしゃる")) {
         return uc("おっしゃれば");
      }
     if (type(verb, ruVerb) == Type::RU_RU) {
         return joinpop(imperative_form(verb, ruVerb), 1, "れば");
      }
      else {
         return join(imperative_form(verb, ruVerb), "ば");
      }
   }

   static UnicodeString te_form_positive(const UnicodeString& verb, bool ruVerb)
   {
      if (verb == uc("する")) {
         return uc("して");
      }
      if (verb == uc("くる") || verb == uc("来る")) {
         return uc("きて");
      }
      if (verb == uc("行く")) {
         return uc("行って");
      }

      UnicodeString ext;
      switch (type(verb, ruVerb)) {
         case Type::KU:
            ext = "いて";
            break;
         case Type::GU:
            ext = "いで";
            break;
         case Type::BU:
         case Type::MU:
         case Type::NU:
            ext = "んで";
            break;
         case Type::RU:
         case Type::U:
         case Type::TSU:
            ext = "って";
            break;
         case Type::RU_RU:
            ext = "て";
            break;
         case Type::SU:
            ext = "して";
            break;
      }

      return join(stem(verb), ext);
   }

   static UnicodeString te_form_negative(const UnicodeString& verb, bool ruVerb)
   {
      return joinpop(simple_present_negative(verb, ruVerb), 1, "くて");
   }

   static UnicodeString simple_volitional_positive(const UnicodeString& verb, bool ruVerb)
   {
      if (verb == uc("する")) {
         return uc("しよう");
      }
      if (verb == uc("くる") || verb == uc("来る")) {
         return uc("こよう");
      }

      ::std::string ext;
      switch (type(verb, ruVerb)) {
         case Type::BU:
            ext = "ぼう";
            break;
         case Type::GU:
            ext = "ぐう";
            break;
         case Type::KU:
            ext = "こう";
            break;
         case Type::MU:
            ext = "もう";
            break;
         case Type::NU:
            ext = "のう";
            break;
         case Type::RU:
            ext = "ろう";
            break;
         case Type::RU_RU:
            ext = "よう";
            break;
         case Type::SU:
            ext = "そう";
            break;
         case Type::TSU:
            ext = "とう";
            break;
         case Type::U:
            ext = "おう";
            break;
      }
      return join(stem(verb), ext);
   }

   static UnicodeString polite_present_positive(const UnicodeString& verb, bool ruVerb)
   {
      if (verb == uc("する")) {
         return uc("します");
      }
      if (verb == uc("くる") || verb == uc("来る")) {
         return uc("きます");
      }

      ::std::string ext;
      switch (type(verb, ruVerb)) {
         case Type::BU:
            ext = "びます";
            break;
         case Type::GU:
            ext = "ぎます";
            break;
         case Type::KU:
            ext = "きます";
            break;
         case Type::MU:
            ext = "みます";
            break;
         case Type::NU:
            ext = "にます";
            break;
         case Type::RU:
            ext = "ります";
            break;
         case Type::RU_RU:
            ext = "ます";
            break;
         case Type::SU:
            ext = "します";
            break;
         case Type::TSU:
            ext = "ちます";
            break;
         case Type::U:
            ext = "います";
            break;
      }
      return join(stem(verb), ext);
   }
   static UnicodeString polite_volitional_positive(const UnicodeString& verb, bool ruVerb)
   {
      return joinpop(polite_present_positive(verb, ruVerb), 1, "しょう");
   }

   static UnicodeString polite_present_negative(const UnicodeString& verb, bool ruVerb)
   {
      return joinpop(polite_present_positive(verb, ruVerb), 1, "せん");
   }

   static UnicodeString passive_form(const UnicodeString& verb, bool ruVerb)
   {
      if (verb == uc("ある")) {
         return uc("");
      }
      if (verb == uc("する")) {
         return uc("される");
      }
      if (verb == uc("くる") || verb == uc("来る")) {
         return uc("こられる");
      }
      if (type(verb, ruVerb) == Type::RU_RU) {
         return potential_form(verb, ruVerb);
      }
      else {
         return joinpop(simple_present_negative(verb, ruVerb), 2, "れる");
      }
   }

   static UnicodeString causative_form(const UnicodeString& verb, bool ruVerb)
   {
      if (verb == uc("ある")) {
         return uc("");
      }
      if (verb == uc("する")) {
         return uc("させる");
      }
      if (verb == uc("くる") || verb == uc("来る")) {
         return uc("こさせる");
      }
      if (type(verb, ruVerb) == Type::RU_RU) {
         return join(stem(verb), "させる");
      }
      else {
         return joinpop(simple_present_negative(verb, ruVerb), 2, "せる");
      }
   }

   ::std::vector< ::std::string> conjugateVerb(const ::std::string& utf8, bool ruVerb)
   {
      const UnicodeString verb(utf8);
      ::std::vector < ::std::string > result;

      const size_t n = verb.length();
      const UnicodeString stem = verb.substring(0, n - 1);
      const UnicodeString tail = verb.substring(n - 1, 1);

      result.push_back(stem.string());
      result.push_back(type(verb, ruVerb) == Type::RU_RU ? "る" : "う");
      result.push_back(simple_present_positive(verb, ruVerb).string());
      result.push_back(simple_present_negative(verb, ruVerb).string());
      result.push_back(simple_past_positive(verb, ruVerb).string());
      result.push_back(simple_past_negative(verb, ruVerb).string());
      result.push_back(polite_present_positive(verb, ruVerb).string());
      result.push_back(polite_present_negative(verb, ruVerb).string());
      result.push_back(te_form_positive(verb, ruVerb).string());
      result.push_back(te_form_negative(verb, ruVerb).string());
      result.push_back(simple_volitional_positive(verb, ruVerb).string());
      result.push_back(polite_volitional_positive(verb, ruVerb).string());
      result.push_back(potential_form(verb, ruVerb).string());
      result.push_back(conditional_form(verb, ruVerb).string());
      result.push_back(passive_form(verb, ruVerb).string());
      result.push_back(causative_form(verb, ruVerb).string());
      result.push_back(imperative_form(verb, ruVerb).string());

      return result;
   }
}

