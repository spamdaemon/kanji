#include <kanji/jmdict/Entry.h>
#include <kanji/jmdict/PartOfSpeech.h>
#include <iostream>
#include <cstring>

using namespace kanji::jmdict;

size_t maxNumEntries = 0;
size_t numEntries = 0;

static int getScore(const kanji::jmdict::Entry& e)
{
   for (auto& k : e.kanji) {
      for (auto& ext : k.ext) {
         if (ext.field == "kscore") {
            return ext.number;
         }
      }
   }
   return 0;
}

static bool isGoodSense(const Sense& s)
{
   for (auto pos : s.pos) {
      if (!::kanji::jmdict::isProperWord(pos)) {
         return false;
      }
   }
   return true;
}

static void processEntry(Entry& e)
{
   if (maxNumEntries != 0 && numEntries == maxNumEntries) {
      return;
   }
   if (getScore(e)>maxNumEntries) {
           return;
   }

   for (const Kanji& k : e.kanji) {
      if (k.keb.empty()) {
         continue;
      }
      ++numEntries;
      for (const Reading& r : e.reading) {
         for (const Sense& s : e.sense) {
            if (!isGoodSense(s)) {
               return;
            }
         }
         ::std::cout << k.keb << '\t' << r.reb << '\t' << e.sense[0].gloss[0].gloss << ::std::endl;
         return;
      }
   }
}

int main(int argc, char * const * argv)
{
   for (int i = 1; i < argc; ++i) {
      if (::std::strcmp("-e", argv[i]) == 0) {
         maxNumEntries = atoi(argv[++i]);
      }
   }

   try {
      readEntries(nullptr, processEntry);
   }
   catch (...) {
      ::std::cerr << "Failed to process entries" << ::std::endl;
      return 1;
   }
   return 0;
}
