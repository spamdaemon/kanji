#include <kanji/jmdict/Entry.h>
#include <kanji/parse.h>

#include <iostream>
#include <sstream>
#include <fstream>
#include <string>
#include <memory>
#include <vector>
#include <map>

using namespace kanji::jmdict;

static bool processEntry(const ::std::map< ::std::string, ::std::vector< ::std::string> >& tobira,
      kanji::jmdict::Entry& e)
{
   const Text tobiraText("tobira");
   for (auto j = e.kanji.begin(); j != e.kanji.end(); ++j) {
      Kanji& k = *j;

      auto i = tobira.find(k.keb);
      if (i != tobira.end()) {
         k.ke_pri.push_back(tobiraText);
      }
   }

   for (auto j = e.reading.begin(); j != e.reading.end(); ++j) {
      Reading& k = *j;
      auto i = tobira.find(k.reb);
      if (i != tobira.end()) {
         k.re_pri.push_back(tobiraText);
      }
   }
   return true;
}

int main(int argc, char * const * argv)
{
// read the core 10K file
   if (argc < 2) {
      ::std::cerr << "Missing tobira file" << ::std::endl;
      return 1;
   }

   ::std::map< ::std::string, ::std::vector< ::std::string> > tobira;
   ::std::ifstream stream(argv[1]);
   while (stream && !stream.eof()) {
      ::std::vector < ::std::string > fields = kanji::readDelimitedLine(stream, "\t");
      if (fields.size() > 3) {
         tobira[fields[0]] = fields;
      }
   }
   if (tobira.empty()) {
      ::std::cerr << "No tobira file read" << ::std::endl;
      return 1;
   }
   try {
      readEntries(nullptr, [&] (Entry& e) {
         if (processEntry(tobira,e)) {
            ::std::cout << e;
         }
      });
   }
   catch (...) {
      ::std::cerr << "Failed to process entries" << ::std::endl;
      return 1;
   }
   return 0;
}
