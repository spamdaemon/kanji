#include <kanji/parse.h>
#include <timber/text/unicode/UnicodeString.h>
#include <timber/text/unicode/UTF8Coding.h>
#include <timber/text/unicode/UCD.h>

#include <cstring>
#include <iostream>
#include <fstream>
#include <string>
#include <map>
#include <cstdlib>

using namespace timber::text::unicode;

// score jpn text based on the kanji with the largest number
// if the str contains a kanji that is not known, then a score of -1 is returned
static int scoreText(const std::map< CodePoint, int>& kanjis, ::std::string& str)
{
   int score = 0;
   const UnicodeString text(str);
   for (size_t i = 0; i < text.length(); ++i) {
      auto kanjiCP = text[i];
      if (::std::strstr(UCD::blockOf(kanjiCP), "CJK") != nullptr) {
         auto p = kanjis.find(kanjiCP);
         if (p == kanjis.end()) {
            score = -1;
            break;
         }

         if (p->second > score) {
            kanjiCP = text[i];
            score = p->second;
         }
      }
   }
   return score;
}

int main(int argc, char * const * argv)
{
   std::map< CodePoint, int> scores;

   if (argc < 3) {
      ::std::cerr << "Missing kanji file";
   }

   // load the scores
   {
      ::std::ifstream stream(argv[1]);
      ::std::string line;
      ::std::vector < std::string > fields;
      while (stream && !stream.eof()) {
         fields = kanji::readCSVLine(stream);
         if (fields.size() >= 2 && !fields[0].empty() && !fields[1].empty()) {
            int index = atoi(fields[0].c_str());
            UnicodeString str(fields[1]);
            str = UnicodeString(str[0]);
            if (::std::strstr(UCD::blockOf(str[0]), "CJK") != nullptr) {
               scores[str[0]] = index;
//                  ::std::cerr << index << '\t' << str.utf8() << '\t' << UCD::blockOf(str[0]) << ::std::endl;
            }
            else {
               ::std::cerr << "Not a CJK UNIFIED IDEOGRAPH: " << index << '\t' << str.utf8() << '\t'
                     << UCD::blockOf(str[0]) << ::std::endl;
            }
         }
      }
   }

   size_t fieldIndex = 0;
   if (argc >=3 ) {
      fieldIndex=(size_t)atoi(argv[2]);
   }

   try {
      ::std::vector < std::string > fields;
      while (true) {
         fields = kanji::readDelimitedLine(::std::cin,"\t");
         if (!::std::cin) {
            break;
         }
         if (fieldIndex>=fields.size()) {
            continue;
         }
         int score = scoreText(scores, fields[fieldIndex]);
         if (score < 0) {
            // text contains unknown kanji
            // so ignore it
         }
         else if (score == 0) {
            // text contains no kanji at all
            // so ignore it
         }
         else {
            ::std::cout << score;
            for (const ::std::string& text : fields) {
               ::std::cout << '\t' << text;
            }
            ::std::cout << ::std::endl;
         }
      }
   }
   catch (...) {
      ::std::cerr << "Failed to process entries" << ::std::endl;
      return 1;
   }
   return 0;
}
