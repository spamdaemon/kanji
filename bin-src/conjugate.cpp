#include <kanji/conjugate.h>
#include <kanji/parse.h>
#include <fstream>
#include <vector>
#include <iostream>
#include <string>

int main(int argc, char * const * argv)
{
   int argpos=1;
   ::std::ifstream stream(argpos < argc ? argv[argpos++] : "/dev/stdin");

   ::std::cout
       << "#stem\t"
       <<"type\t"
       <<"inf.nonpast\t"
       <<"inf.neg.nonpast\t"
       <<"inf.past\t"
       <<"inf.neg.past\t"
       <<"formal.nonpast\t"
       <<"formal.neg.nonpast\t"
       <<"te-form\t"
       <<"te-form.neg\t"
       <<"volitional\t"
       <<"formal.volitional\t"
       <<"potential\t"
       <<"conditional\t"
       <<"passive\t"
       <<"causative\t"
       <<"imperative"
       << ::std::endl;

   while (stream && !stream.eof()) {
      auto fields = kanji::readDelimitedLine(stream, "\t");
      if (fields.size() >0) {
         bool ruVerb = fields.size() == 2 && fields[1] == "る";
         const char* sep = "";
         try {
            for (auto v : kanji::conjugateVerb(fields[0], ruVerb)) {
               ::std::cout << sep << v;
               sep = "\t";
            }
            ::std::cout << ::std::endl;
         }
         catch (...) {
            ::std::cerr << "failed to conjugate " << fields[0] << ::std::endl;
         }
      }
   }
   return 0;
}
