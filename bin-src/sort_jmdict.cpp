#include <kanji/jmdict/Entry.h>

#include <cstring>
#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <memory>
#include <map>
#include <cstdlib>

using namespace kanji::jmdict;

static int getScore(const Entry& e)
{
   for (const Kanji& k : e.kanji) {
      for (const Extension& ext : k.ext) {
         if (ext.field == "kscore") {
            return ext.number;
         }
      }
   }
   return 0;
}

int main(int argc, char * const * argv)
{

   ::std::unique_ptr < ::std::ifstream > file;
   ::std::istream* stream = nullptr;
   if (argc != 2) {
      ::std::cerr << "No file specified; reading from stdin" << ::std::endl;
      stream = &::std::cin;
   }
   else {
      file.reset(new ::std::ifstream(argv[1]));
      stream = file.get();
   }
   kanji::jmdict::Entry e;
   try {
      std::multimap< int, RawEntry> scores;
      while (*stream) {
         RawEntry entry(readRawEntry(*stream));
         if (entry.n == 0) {
            break;
         }
         ::std::istringstream es(::std::string(entry.buffer.get(), entry.n));
         es >> e;
         if (es.fail()) {
            break;
         }
         int score = getScore(e);
         if (score > 0) {
            scores.emplace(score,::std::move(entry));
         }
         else {
            ::std::cerr << "No score " << e.ent_seq << ::std::endl;
         }
      }
      for (auto& et : scores) {
         ::std::cout.write(et.second.buffer.get(), et.second.n);
      }
   }
   catch (...) {
      ::std::cerr << "Failed to process entries" << ::std::endl;
      return 1;
   }
   return 0;
}
