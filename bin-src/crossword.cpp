#include <iostream>
#include <tuple>
#include <vector>
#include <string>
#include <random>
#include <algorithm>
#include <set>
#include <utility>

#include <kanji/parse.h>
#include <timber/text/unicode/CodePoint.h>
#include <timber/text/unicode/UnicodeString.h>
#include <timber/text/unicode/UTF32String.h>

using namespace ::timber;
using namespace ::timber::text::unicode;

template<class X, class ...T>
static void scrambleVector(::std::vector< X, T...>& v, std::default_random_engine& generator)
{
   ::std::uniform_int_distribution < size_t > dice(size_t(0), v.size() - 1);
   for (size_t i = 0; i < v.size(); ++i) {
      const size_t j = dice(generator);
      ::std::swap(v[i], v[j]);
   }
}

class Grid
{
      /** The index into a cell in the grid */
   private:
      typedef ::std::pair< size_t, size_t> GridIndex;

      /** The index into a cell in the grid */
   private:
      typedef ::std::pair< int, int> Direction;

      /** A viable insertion at a grid point in a given direction */
   private:
      typedef ::std::pair< GridIndex, Direction> ViableInsertion;

   public:
      Grid(size_t w, size_t h)
            : _width(w), _height(h), _grid(w * h, CodePoint(0))
      {
         _directions.push_back(Direction(1, 0));
         _directions.push_back(Direction(0, 1));
         // _directions.push_back(Direction(1, 1));
      }

   public:
      ~Grid()
      {
      }

   public:
      void print(::std::ostream& out) const
      {
         UTF32Coding utf32;
         for (size_t i = 0; i < _width + 2; ++i) {
            out << '-';
         }
         out << ::std::endl;
         for (GridIndex i(0, 0); i.first < _height; ++i.first) {
            out << '|';
            for (i.second = 0; i.second < _width; ++i.second) {
               CodePoint cp = getCP(i);
               if (cp == 0) {
                  out << ' ';
               }
               else {
                  utf32.print(out, &cp, 1);
               }
            }
            out << '|';
            out << ::std::endl;
         }
         for (size_t i = 0; i < _width + 2; ++i) {
            out << '-';
         }
         out << ::std::endl;
      }
      void printHTML(::std::ostream& out, const ::std::string& title, ::std::set< UnicodeString>& words) const
      {

         UTF32Coding utf32;
         out << "<html>" << ::std::endl;
         out << "<head>" << ::std::endl;
         out << "<title>" << title << "</title>" << ::std::endl;
         out << "</head>" << ::std::endl;
         out << "<body>" << ::std::endl;
         out << "<div>" << ::std::endl;
         out << "<ol>" << ::std::endl;
         for (auto w : words) {
            out << "<li>" << w.utf8() << "</li>" << ::std::endl;
         }
         out << "</ol>" << ::std::endl;
         out << ::std::endl;
         out << "</div>" << ::std::endl;
         out << "<table>" << ::std::endl;
         for (GridIndex i(0, 0); i.first < _height; ++i.first) {
            out << "<tr>" << ::std::endl;
            for (i.second = 0; i.second < _width; ++i.second) {
               out << "<td>";
               CodePoint cp = getCP(i);
               if (cp == 0) {
                  out << ' ';
               }
               else {
                  utf32.print(out, &cp, 1);
               }
               out << "</td>" << ::std::endl;
            }
            out << "</tr>" << ::std::endl;
         }
         out << "</body>" << ::std::endl;
         out << "</html>" << ::std::endl;
      }

      /**
       * Insert a new word into the grid
       */
   public:
      bool insert(const UnicodeString& text)
      {
         const UTF32String& utf32 = text.utf32();
         if (utf32.isEmpty()) {
            return false;
         }
         auto locations = findViableInsertions(utf32.characterAt(0));
         scrambleVector(locations, _generator);
         while (!locations.empty()) {
            // take the item at the end of the vector and insert it,
            if (insertText(utf32, locations.back().first, locations.back().second)) {
               return true;
            }
            // need to remoe the item that we could not insert
            locations.pop_back();
         }

         return false;
      }

      /**
       * Fill the grid randomly with code points.
       * @param fill codepoints to fill the grid with
       */
   public:
      void fill(const ::std::vector< CodePoint>& cp)
      {
         ::std::uniform_int_distribution< size_t> dice(size_t(0), cp.size() - 1);
         for (GridIndex i(0, 0); i.first < _height; ++i.first) {
            for (i.second = 0; i.second < _width; ++i.second) {
               if (isAvailable(CodePoint(0), i)) {
                  setCP(cp.at(dice(_generator)), i);
               }
            }
         }
      }

      /**
       * Test if the specified text can be inserted. If the text cannot be inserted,
       * no change is made to the grid.
       * @param text a text
       * @param i a grid index
       * @param dRow the number of rows to move (0,1)
       * @param dCol the number of cols to move (0,1)
       * @return true if the text placed into the grid
       */
   private:
      bool insertText(const UTF32String& text, const GridIndex& index, const Direction& direction)
      {
         // need test if we can insert
         {
            GridIndex i(index);
            for (size_t k = 0; k < text.length(); ++k, i.first += direction.first, i.second += direction.second) {
               if (!isValidIndex(i)) {
                  return false;
               }
               if (!isAvailable(text.characterAt(k), i)) {
                  return false;
               }
            }
         }

         // ok, insert the text
         {
            GridIndex i(index);
            for (size_t k = 0; k < text.length(); ++k, i.first += direction.first, i.second += direction.second) {
               setCP(text.characterAt(k), i);
            }
         }
         return true;
      }

      /**
       * Find a list of viable locations where the specified code point
       * can be inserted.
       * @param cp a code point
       * @return a vector indexes
       */
   private:
      ::std::vector< ViableInsertion> findViableInsertions(CodePoint cp) const throws()
      {
         ::std::vector< ViableInsertion> res;
         for (GridIndex i = { 0, 0 }; i.first < _height; ++i.first) {
            for (i.second = 0; i.second < _width; ++i.second) {
               if (isAvailable(cp, i)) {
                  for (GridIndex d : _directions) {
                     res.push_back(ViableInsertion(i, d));
                  }
               }
            }
         }
         return res;
      }

      /**
       * Test if the specified code point fits into the given cell.
       * @param cp a code point
       * @param i row column index
       * @return true if the code point fits into the grid
       */
   private:
      bool isAvailable(CodePoint cp, const GridIndex& i) const throws()
      {
         CodePoint p = getCP(i);
         return cp == p || p == 0;
      }

      /**
       * Test if the index is a valid index.
       * @param i an index
       * @return true if points at a valid cell
       */
   private:
      bool isValidIndex(const GridIndex& i) const throws()
      {
         return i.first < _height && i.second < _width;
      }

      /**
       * Set the grid point to the given cp
       * @param cp a the code point
       * @param row a row
       * @param col a column
       */
   private:
      void setCP(CodePoint cp, const GridIndex& i) throws()
      {
         CodePoint& p = _grid.at(i.first * _width + i.second);
         assert(p == 0 || p == cp);
         p = cp;
      }

      /**
       * Get a code point in a grid cell
       * @param row a row
       * @param col a column
       */
   public:
      CodePoint getCP(const GridIndex& i) const
      {
         return _grid.at(i.first * _width + i.second);
      }

      /** The grid */
   private:
      size_t _width;

      /** The height */
   private:
      size_t _height;

      /** The grid */
   private:
      ::std::vector< CodePoint> _grid;

      /** A random number generator */
   private:
      std::default_random_engine _generator;

      /** The available directions */
   private:
      ::std::vector< Direction> _directions;
}
;

::std::ostream& operator<<(::std::ostream& out, const Grid& g)
{
   g.print(out);
   return out;
}

static ::std::string trimQuotes(const ::std::string& str)
{
   ::std::string res(str);
   if (!res.empty() && *res.begin() == '"') {
      res.erase(res.begin());
   }
   if (!res.empty() && *res.rbegin() == '"') {
      res.erase(res.begin() + res.size() - 1);
   }

   return res;
}

int main(int argc, char** argv)
{
   bool kanjiInGrid = false;
   size_t n = 15;
   size_t N = 20;
   size_t seed = 0;

   try {
      for (int i = 1; i < argc; ++i) {
         ::std::string arg(argv[i]);
         if (arg == "-k") {
            kanjiInGrid = true;
         }
         else if (arg == "-s") {
            if (i+1 == argc) {
               throw ::std::runtime_error("Missing argument for "+arg);
            }
            arg = argv[++i];
            n = ::std::stoul(arg);
            if (n > 30) {
               ::std::cerr << "Grid too large: " << n << ::std::endl;
               return 1;
            }
            else if (n < 5) {
               ::std::cerr << "Grid is too small: " << n << ::std::endl;
               return 1;
            }
         }
         else if (arg == "-n") {
            if (i+1 == argc) {
               throw ::std::runtime_error("Missing argument for "+arg);
            }
            arg = argv[++i];
            N = ::std::stoul(arg);
            if (N == 0) {
               ::std::cerr << "N is too small: " << N << ::std::endl;
               return 1;
            }
         }
         else if (arg == "-r") {
            if (i+1 == argc) {
               throw ::std::runtime_error("Missing argument for "+arg);
            }
            arg = argv[++i];
            seed = ::std::stoul(arg);
         }
      }
   }
   catch (const ::std::exception& e) {
      ::std::cerr << e.what() << ::std::endl;
      return 1;
   }
   std::default_random_engine generator(seed);

   Grid grid(n, n);
   // read a list of words and insert each
   ::std::set< UnicodeString> ignore;

   ::std::vector< CodePoint> fill;
   ::std::vector< ::std::pair< UnicodeString, UnicodeString>> words;

   while (!::std::cin.eof()) {
      ::std::vector < ::std::string > line = ::kanji::readDelimitedLine(::std::cin, "\t");
      if (line.empty()) {
         continue;
      }
      if (line[0] == "character") {
         const UnicodeString str(trimQuotes (line[2]));
         if (kanjiInGrid || line[1] != "kanji") {
            if (str.length() == 1) {
               fill.push_back(str.codePoint(0));
            }
         }
      }
      else if (line[0] == "word") {
         const UnicodeString kanji(trimQuotes (line[1]));
         const UnicodeString kana(trimQuotes (line[2]));
         const UnicodeString sense(trimQuotes (line[3]));

         ::std::pair< UnicodeString, UnicodeString> wordPair;

         if (kanjiInGrid) {
            wordPair.first = kanji;
            wordPair.second = kana;
         }
         else {
            wordPair.first = kana;
            wordPair.second = kanji;
         }
         words.push_back(wordPair);
      }
   }

   scrambleVector(words, generator);
   ::std::set< UnicodeString> wordlist;
   for (auto word : words) {
      auto forGrid = word.first;
      auto forWordlist = word.second;

      if (ignore.count(forGrid) != 0 || ignore.count(forWordlist) != 0) {
         continue;
      }

      if (grid.insert(forGrid)) {
         wordlist.insert(forWordlist+UnicodeString(" ") +forGrid);
         if (wordlist.size() == N) {
            break;
         }
      }
      ignore.insert(forGrid);
      ignore.insert(forWordlist);
   }
   grid.fill(fill);
   grid.printHTML(::std::cout, "Crossword Puzzle", wordlist);

   return 0;
}
