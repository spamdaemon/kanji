#include <kanji/jmdict/Entry.h>
#include <kanji/parse.h>
#include <timber/text/unicode/UnicodeString.h>
#include <timber/text/unicode/UCD.h>

#include <iostream>
#include <fstream>
#include <string>
#include <map>
#include <set>

using namespace std;

struct Kanji
{
      int index;
      ::std::string kanji;
      ::timber::text::unicode::CodePoint cp;
};

static ::std::map< int, Kanji> createKanji(const ::std::string& file)
{
   ::std::map< int, Kanji> result;
   ::std::ifstream stream(file);
   ::std::string line;
   ::std::vector < std::string > fields;
   while (stream && !stream.eof()) {
      fields = kanji::readCSVLine(stream);
      if (fields.size() >= 2 && !fields[0].empty() && !fields[1].empty()) {
         Kanji kanji;
         kanji.index = atoi(fields[0].c_str());
         kanji.kanji = fields[1];
         ::timber::text::unicode::UnicodeString str(fields[1]);
         str = ::timber::text::unicode::UnicodeString(str[0]);
         if (::std::strstr(::timber::text::unicode::UCD::blockOf(str[0]), "CJK") != nullptr) {
            kanji.cp = str[0];
            result[kanji.index] = kanji;
         }
      }
   }
   return ::std::move(result);
}

static void getKanjis(const ::std::string& text, ::std::set< ::timber::text::unicode::CodePoint>& res)
{
   ::timber::text::unicode::UnicodeString str(text);
   for (size_t i = 0; i < str.length(); ++i) {
      if (::std::strstr(::timber::text::unicode::UCD::blockOf(str[i]), "CJK") != nullptr) {
         res.insert(str[i]);
      }
   }
}

static bool filterSentence(const ::std::string& text, const ::std::set< ::timber::text::unicode::CodePoint>& kanji)
{
   ::std::set< ::timber::text::unicode::CodePoint> cps;
   getKanjis(text, cps);
   for (auto& cp : cps) {
      if (kanji.count(cp) == 0) {
         return false;
      }
   }
   return true;
}

int main(int argc, char * const * argv)
{
   ::std::map< int, Kanji> allKanji;
   ::std::set< ::timber::text::unicode::CodePoint> kanji;
   int argpos = 1;
   if (argc <= argpos) {
      ::std::cerr << "Warning: missing kanji file" << ::std::endl;
   }
   else {
      allKanji = createKanji(argv[argpos++]);
      for (auto& k : allKanji) {
         ::timber::text::unicode::UnicodeString str(k.second.kanji);
         kanji.insert(str[0]);
      }
   }

   ::std::ifstream stream(argpos < argc ? argv[argpos++] : "/dev/stdin");
   ::std::vector < std::string > fields;
   int status = 1;
   while (stream && !stream.eof()) {
      fields = kanji::readDelimitedLine(stream, "\t");
      if (!fields.empty() && fields.size() > 2 && fields[1] == "jpn") {
         if (filterSentence(fields[2], kanji)) {
            ::std::cout << fields[2] << ::std::endl;
            status = 0;
         }
      }
   }

   return status;
}

