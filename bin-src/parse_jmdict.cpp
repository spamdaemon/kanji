#include <kanji/jmdict/Entry.h>
#include <kanji/jmdict/Parser.h>

#include <iostream>
#include <fstream>
#include <string>
#include <memory>

static bool processEntry(const kanji::jmdict::Entry& e)
{
   ::std::cout << e;
   return true;
}

int main(int argc, char * const * argv)
{
   ::std::unique_ptr < ::std::ifstream > file;
   ::std::istream* stream = nullptr;
   if (argc != 2) {
      ::std::cerr << "No file specified; reading from stdin" << ::std::endl;
      stream = &::std::cin;
   }
   else {
      file.reset(new ::std::ifstream(argv[1]));
      stream = file.get();
   }
   kanji::jmdict::Parser parser;
   try {
      parser.parseXml(*stream, processEntry);
   }
   catch (const ::std::exception& e) {
      ::std::cerr << "Caught exception " << e.what() << ::std::endl;
      return 1;
   }
   return 0;
}
