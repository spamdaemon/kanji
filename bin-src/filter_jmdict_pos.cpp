#include <kanji/jmdict/Entry.h>
#include <kanji/jmdict/Sense.h>

#include <fstream>
#include <iostream>
#include <memory>
#include <set>
#include <string>

using namespace kanji::jmdict;

static bool processEntry(const std::set< Text>& postype, kanji::jmdict::Entry& e)
{
   for (auto i = e.sense.begin(); i != e.sense.end();) {
      auto pos = typeOf(*i);
      bool keepSense = false;
      for (auto t : postype) {
         switch (pos) {
            case SenseType::ADJECTIVE:
               if (t == "adjective") {
                  keepSense = true;
               }
               break;
            case SenseType::ADVERB:
               if (t == "adverb") {
                  keepSense = true;
               }
               break;
            case SenseType::NOUN:
               if (t == "noun") {
                  keepSense = true;
               }
               break;
            case SenseType::PARTICLE:
               if (t == "particle") {
                  keepSense = true;
               }
               break;
            case SenseType::VERB:
               if (t == "verb") {
                  keepSense = true;
               }
               break;
            default:
               break;
         }
         if (keepSense) {
            break;
         }
      }
      if (!keepSense) {
         e.sense.erase(i);
      }
      else {
         ++i;
      }
   }
   return !e.sense.empty();
}

int main(int argc, char * const * argv)
{
   std::set< Text> pri;
   for (int i = 1; i < argc; ++i) {
      pri.insert(argv[i]);
   }
   try {
      readEntries(nullptr, [&] (Entry& e) {
         if (processEntry(pri,e)) {
            ::std::cout << e;
         }
      });
   }
   catch (...) {
      ::std::cerr << "Failed to process entries" << ::std::endl;
      return 1;
   }
   return 0;
}
