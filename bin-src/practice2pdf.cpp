#include <kanji/jmdict/Entry.h>
#include <kanji/parse.h>
#include <timber/text/unicode/UnicodeString.h>
#include <timber/text/unicode/UCD.h>
#include <iostream>
#include <fstream>
#include <set>
#include <map>

#include <cairo/cairo.h>
#include <cairo/cairo-pdf.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <random>

using namespace std;

struct Kanji
{
      int index;
      ::std::string kanji;
      ::timber::text::unicode::CodePoint cp;
      ::std::vector< ::std::string> readings;
      ::std::vector< kanji::jmdict::Entry> entries;
};
static bool isValidSense(const kanji::jmdict::Entry& E, const kanji::jmdict::Sense& sense)
{
   if (!sense.lsource.empty()) {
      return false;
   }
   for (auto x : sense.stagk) {
      if (x != E.kanji[0].keb) {
         return false;
      }
   }
   for (auto x : sense.misc) {
      if (x == "archaism") {
         return false;
      }
      if (x == "obscure term") {
         return false;
      }
      if (x == "obsolete term") {
         return false;
      }
      if (x == "abbreviation") {
         return false;
      }
   }
   return true;
}

static bool isValidEntry(const kanji::jmdict::Entry& E, size_t nSenses)
{
   for (const kanji::jmdict::Extension& e : E.kanji[0].ext) {
      if (e.field == "alternative") {
         return false;
      }
   }
   for (size_t i = 0; i < E.sense.size() && i < nSenses; ++i) {
      if (isValidSense(E, E.sense[i])) {
         return true;
      }
   }
   return false;
}
static bool removeInvalidSenses(kanji::jmdict::Entry& E)
{
   for (auto i = E.sense.begin(); i != E.sense.end();) {
      if (isValidSense(E, *i)) {
         ++i;
      }
      else {
         E.sense.erase(i);
      }
   }
   return !E.sense.empty();
}

static void getKanjis(const ::std::string& text, ::std::set< ::timber::text::unicode::CodePoint>& res)
{
   ::timber::text::unicode::UnicodeString str(text);
   for (size_t i = 0; i < str.length(); ++i) {
      if (::std::strstr(::timber::text::unicode::UCD::blockOf(str[i]), "CJK") != nullptr) {
         res.insert(str[i]);
      }
   }
}

static ::std::vector< ::std::string> splitText(const ::std::string& text, const char* sep)
{
   ::std::vector < ::std::string > words;
   int sz = strlen(sep);

   ::std::string::size_type pos = 0;
   ::std::string::size_type npos;
   while (pos < text.length() && (npos = text.find(sep, pos)) != ::std::string::npos) {
      ::std::string str(text.substr(pos, npos - pos));
      if (!str.empty()) {
         words.push_back(str);
      }
      pos = npos + sz;
   }
   if (pos < text.length()) {
      ::std::string str(text.substr(pos));
      if (!str.empty()) {
         words.push_back(str);
      }
   }
   return ::std::move(words);
}

static int getScore(const kanji::jmdict::Entry& e)
{
   for (auto& k : e.kanji) {
      for (auto& ext : k.ext) {
         if (ext.field == "kscore") {
            return ext.number;
         }
      }
   }
   return 0;
}

static ::std::string getPriority(const kanji::jmdict::Entry& e)
{
   ::std::string pri;
   for (auto& k : e.kanji) {
      for (::std::string p : k.ke_pri) {
         if (p.empty()) {
            continue;
         }
         if (p.find("core2k") == 0) {
            p = ::std::string("0") + p;
         }
         else if (p.find("core4k") == 0) {
            p = ::std::string("1") + p;
         }
         else if (p.find("core6k") == 0) {
            p = ::std::string("2") + p;
         }
         else if (p.find("tobira") == 0) {
            p = ::std::string("00") + p;
         }
         else if (p.find("core10k") == 0) {
            p = ::std::string("1") + p;
         }
         else if (p.find("ichi") == 0) {
            p = ::std::string("b") + p;
         }
         else if (p.find("news") == 0) {
            p = ::std::string("c") + p;
         }
         else if (p.find("spec") == 0) {
            p = ::std::string("d") + p;
         }
         else if (p.find("nf") == 0) {
            p = ::std::string("a") + p;
         }
         else {
            continue;
         }
         if (pri.empty() || p < pri) {
            pri = ::std::move(p);
         }
      }
   }
   return pri;
}

static ::std::map< int, Kanji> createKanji(const ::std::string& file)
{
   ::std::map< int, Kanji> result;
   ::std::ifstream stream(file);
   ::std::string line;
   ::std::vector < std::string > fields;
   while (stream && !stream.eof()) {
      fields = kanji::readCSVLine(stream);
      if (fields.size() >= 2 && !fields[0].empty() && !fields[1].empty()) {
         Kanji kanji;
         kanji.index = atoi(fields[0].c_str());
         kanji.kanji = fields[1];
         kanji.readings = splitText(fields[2], " ・ ");
         ::timber::text::unicode::UnicodeString str(fields[1]);
         str = ::timber::text::unicode::UnicodeString(str[0]);
         if (::std::strstr(::timber::text::unicode::UCD::blockOf(str[0]), "CJK") != nullptr) {
            kanji.cp = str[0];
            result[kanji.index] = kanji;
         }
      }
   }
   return ::std::move(result);
}

static const double PAGE_WIDTH_IN = 8;
static const double PAGE_HEIGHT_IN = 10.5;

typedef struct
{
      double x, y;
      double w, h;
} KanjiExtent;

typedef struct
{
      double inset;
      double strokeWidth;
      double font_size;
      double text_color;
} KanjiBox;

static double in2pts(double inches)
{
   return inches * 72;
}

static double min(double x, double y)
{
   return x < y ? x : y;
}

static void adjustExtent(KanjiExtent* extent, KanjiBox options)
{
   double inset = options.inset + options.strokeWidth;
   extent->x += inset;
   extent->y += inset;
   extent->w -= 2 * inset;
   extent->h -= 2 * inset;
}
static void unadjustExtent(KanjiExtent* extent, KanjiBox options)
{
   double inset = options.inset + options.strokeWidth;
   extent->x -= inset;
   extent->y -= inset;
   extent->w += 2 * inset;
   extent->h += 2 * inset;
}

static void drawText(cairo_t* c, KanjiExtent extent, KanjiBox options, const ::std::string& utf8)
{
   cairo_text_extents_t extents;
   adjustExtent(&extent, options);
   double fontSize = min(extent.w, extent.h);
   if (options.font_size != 0) {
      fontSize = options.font_size;
   }
   cairo_set_font_size(c, fontSize);
   cairo_scaled_font_t* fnt = cairo_get_scaled_font(c);
   cairo_scaled_font_text_extents(fnt, utf8.c_str(), &extents);

   cairo_move_to(c, extent.x, extent.y + extent.h - (extent.h + extents.y_bearing) / 2);

   cairo_save(c);
   cairo_set_source_rgb(c, options.text_color, options.text_color, options.text_color);
   cairo_show_text(c, utf8.c_str());
   cairo_restore(c);
}

static KanjiExtent drawMultilineText(cairo_t* c, KanjiExtent extent, KanjiBox options,
      const ::std::vector< string>& words, const char* spacer = nullptr, char* linesplits = nullptr)
{
   if (words.empty()) {
      return extent;
   }
   cairo_text_extents_t extents;
   adjustExtent(&extent, options);

   double fontSize = extent.h;
   if (options.font_size != 0) {
      fontSize = options.font_size;
   }

   cairo_set_font_size(c, fontSize);

   cairo_scaled_font_t* fnt = cairo_get_scaled_font(c);

   char buffer[2048];
   int off = 0;
   double y = extent.y + fontSize;
   cairo_move_to(c, extent.x, y);
   buffer[0] = '\0';
   ::std::vector < ::std::string > bufs;
   ::std::string tmp = words[0];
   for (size_t i = 1; i < words.size(); ++i) {
      tmp += spacer ? spacer : " ";
      tmp += words[i];
   }
   bufs = splitText(tmp, linesplits ? linesplits : " ");

   ::std::string xbuf;
   for (auto i = bufs.begin(); i != bufs.end(); ++i) {

      ::std::string tbuf(xbuf);
      if (!tbuf.empty()) {
         tbuf += linesplits ? linesplits : " ";
      }
      tbuf += *i;
      cairo_scaled_font_text_extents(fnt, tbuf.c_str(), &extents);
      if (extent.w < extents.width) {
         cairo_show_text(c, xbuf.c_str());
         xbuf = *i;
         if (i != bufs.begin()) {
            y += fontSize;
         }
         cairo_move_to(c, extent.x, y);
      }
      else {
         xbuf = ::std::move(tbuf);
      }
   }
   if (!xbuf.empty()) {
      cairo_show_text(c, xbuf.c_str());
   }
   extent.h = y - extent.y;
   unadjustExtent(&extent, options);
   return extent;
}

static void drawCharBox(cairo_t* c, KanjiExtent extent, KanjiBox options, const ::std::string& utf8)
{
   // generate the path
   adjustExtent(&extent, options);
   double minx = extent.x;
   double miny = extent.y;
   double maxx = extent.x + extent.w;
   double maxy = extent.y + extent.h;

   cairo_set_line_width(c, options.strokeWidth);
   cairo_set_source_rgb(c, 0, 0, 0);
   cairo_move_to(c, minx, miny);
   cairo_line_to(c, minx, maxy);
   cairo_line_to(c, maxx, maxy);
   cairo_line_to(c, maxx, miny);
   cairo_close_path(c);

   // draw the path using the options
   cairo_stroke(c);

   // draw the text inside the box
   if (!utf8.empty()) {
      drawText(c, extent, options, utf8);
   }
}

static KanjiExtent drawCharBoxRows(cairo_t* c, KanjiExtent extent, KanjiBox options, double sz,
      const ::std::string& utf8 = "")
{
   KanjiExtent ext;
   ext.h = sz;
   ext.w = sz;

   KanjiExtent actual = extent;

   for (ext.x = extent.x; ext.x + ext.w <= extent.x + extent.w; ext.x += ext.w) {
      for (ext.y = extent.y; (extent.y + extent.h) - (ext.y + ext.h) > -.01; ext.y += ext.h) {
         actual.w = ext.x + ext.w - extent.x;
         actual.h = ext.y + ext.h - extent.y;
         drawCharBox(c, ext, options, utf8);
      }
   }

   return actual;
}

static KanjiExtent drawReadings(cairo_t* c, KanjiExtent extent, KanjiBox options,
      const ::std::vector< string>& readings)
{
   return drawMultilineText(c, extent, options, readings, u8"・");
}

// fixme: char** pword needs to be a struct { kanji, kana, meaning }
static KanjiExtent drawPracticeWords(cairo_t* c, KanjiExtent extent, KanjiBox options,
      const ::std::vector< kanji::jmdict::Entry>& pword, std::default_random_engine& generator)
{
   double y = extent.y;

   // pick a random work
   size_t i = 0;
   if (pword.size() > 1) {
      ::std::uniform_int_distribution < size_t > dice(size_t(0), pword.size() - 1);
      i = dice(generator);
   }
   if (!pword.empty()) {
      ::std::vector < ::std::string > words;
      words.push_back(pword[i].reading[0].reb);
      words.push_back(pword[i].sense[0].gloss[0].gloss);
      if (pword[i].sense.size() > 1) {
         words.push_back(pword[i].sense[1].gloss[0].gloss);
      }
      const double h = in2pts(.85);
      const double w = in2pts(1.5);

      KanjiExtent entry = { extent.x, y, w, h };
      if (entry.y + entry.h < extent.y + extent.h) {

         KanjiExtent box = { entry.x, entry.y, entry.w, entry.h };
         KanjiExtent textBox = { box.x + 2, box.y + 2, box.w - 2 * 2, box.h - 2 * 2 };

         // draw the text box
         {
            cairo_save(c);
            adjustExtent(&box, options);

            cairo_set_fill_rule(c, CAIRO_FILL_RULE_EVEN_ODD);
            double minx = box.x;
            double miny = box.y;
            double maxx = box.x + box.w;
            double maxy = box.y + box.h;
            cairo_set_line_width(c, options.strokeWidth);
            cairo_set_source_rgb(c, 0, 0, 0);
            cairo_move_to(c, minx, miny);
            cairo_line_to(c, maxx, miny);
            cairo_line_to(c, maxx, maxy);
            cairo_line_to(c, minx, maxy);
            cairo_close_path(c);
            cairo_stroke(c);

            cairo_move_to(c, minx, miny);
            cairo_line_to(c, maxx, miny);
            cairo_line_to(c, maxx, maxy);
            cairo_line_to(c, minx, maxy);
            cairo_close_path(c);
            cairo_clip(c);
            drawMultilineText(c, textBox, options, words, u8"・ ");
            cairo_restore(c);
         }

         // the practice boxes
         {
            KanjiExtent practiceBoxes = { entry.x + w, entry.y, extent.w - w, h };
            drawCharBoxRows(c, practiceBoxes, options, floor(16384 * (h / 2)) / 16384.0);
         }

         y += h;
      }
   }
   extent.h = y - extent.y;
   return extent;
}

int main(int argc, char** argv)
{
#if CAIRO_HAS_PDF_SURFACE != 1
#error    "PDF surface not available\n";
#endif
   std::default_random_engine generator(0);
   int maxKanjis = 10000;
   int maxSets = 1;
   bool randomize = false;
   int maxKanji = -1;

   ::std::string font("IPAMincho");
   // these practice fonts are good;
   ::std::string practiceFont("mikachan");
   practiceFont = "IPAGothic";
   // this is a handwriting font, which is better practicing kanji
   practiceFont = "S2G moon font";
   ::std::string strokeOrderFont("KanjiStrokeOrders");
   ::std::string output;
   ::std::string kanjiFile("data/kanji.csv");

   double margin = .75;

   for (int i = 1; i < argc; ++i) {
      if (strcmp("-s", argv[i]) == 0) {
         generator = std::default_random_engine(atoi(argv[++i]));
      }
      else if (strcmp("-r", argv[i]) == 0) {
         randomize = true;
      }
      else if (strcmp("-M", argv[i]) == 0) {
         maxKanji = atoi(argv[++i]);
      }
      else if (strcmp("-K", argv[i]) == 0) {
         maxKanjis = atoi(argv[++i]);
      }
      else if (strcmp("-S", argv[i]) == 0) {
         maxSets = atoi(argv[++i]);
      }
      else if (strcmp("-o", argv[i]) == 0) {
         output = argv[++i];
      }
      else if (strcmp("-f", argv[i]) == 0) {
         font = argv[++i];
      }
      else if (strcmp("-p", argv[i]) == 0) {
         practiceFont = argv[++i];
      }
      else if (strcmp("-F", argv[i]) == 0) {
         strokeOrderFont = argv[++i];
      }
      else if (strcmp("-k", argv[i]) == 0) {
         kanjiFile = argv[++i];
      }
      else if (strcmp("-m", argv[i]) == 0) {
         margin = atof(argv[++i]);
      }
   }
   if (margin < 0 || margin > 3 || (margin == 0 && margin != 0)) {
      margin = .75;
   }

   if (font.empty() && !strokeOrderFont.empty()) {
      font = strokeOrderFont;
   }
   else if (!font.empty() && strokeOrderFont.empty()) {
      strokeOrderFont = font;
   }
   if (practiceFont.empty()) {
      practiceFont = font;
   }

   if (output.empty()) {
      output = "/dev/stdout";
   }

   cairo_surface_t* surface = cairo_pdf_surface_create(output.c_str(), in2pts(PAGE_WIDTH_IN), in2pts(PAGE_HEIGHT_IN));
   if (surface == 0) {
      fprintf(stderr, "Failed to create pdf surface\n");
      return 1;
   }

   cairo_t* ctx = cairo_create(surface);
   //IPAMincho
   //mikachan
   //DroidSansJapanese

   // read all the kanji
   map< int, Kanji> kanji = createKanji(kanjiFile);
   map< ::timber::text::unicode::CodePoint, int> kanjiByCp;
   for (auto& x : kanji) {
      kanjiByCp[x.second.cp] = x.first;

   }
   kanji::jmdict::readEntries(::std::cin, [&] (kanji::jmdict::Entry& e) {

      removeInvalidSenses(e);
      if (e.sense.empty()) {
         return;
      }
      if (!isValidEntry(e,100)) {
         return;
      }

      ::std::set<int> scores;

      {
         int s = getScore(e);
         if (s==0) {
            return;
         }
         if (s < maxKanji) {
            ::std::set< ::timber::text::unicode::CodePoint> ustr;
            getKanjis(e.kanji[0].keb, ustr);
            for (auto cp : ustr) {
               if( kanjiByCp.find(cp) != kanjiByCp.end()) {
                  scores.insert(kanjiByCp[cp]);
               }
            }
         }
         if (scores.empty()) {
            scores.insert(s);
         }
      }
      for (auto s : scores) {
         auto i = kanji.find(s);
         if (i!=kanji.end()) {
            auto pri = getPriority(e);
            if (pri.empty()) {
               return;
            }

            for (auto j=i->second.entries.begin();j!=i->second.entries.end();++j) {
               if (!j->kanji[0].keb.empty()) {
                  if (pri < getPriority(*j)) {
                     i->second.entries.insert(j,e);
                     pri.clear();
                     break;
                  }
               }
            }
            if (!pri.empty()) {
               i->second.entries.push_back(e);
            }
            if (i->second.entries.size() > 3) {
               //  i->second.entries.pop_back();
      }
   }
}
});

   map< ::timber::text::unicode::CodePoint, int> kanjiIndex;
   for (auto& i : kanji) {
      ::std::set< ::timber::text::unicode::CodePoint> ustr;
      for (const kanji::jmdict::Entry& e : i.second.entries) {
         getKanjis(e.kanji[0].keb, ustr);
      }
      for (::timber::text::unicode::CodePoint cp : ustr) {
         auto k = kanjiIndex.find(cp);
         if (k == kanjiIndex.end() || i.first < k->second) {
            kanjiIndex[cp] = i.first;
         }
      }
   }

   vector< Kanji> allKanjis;
   for (auto K : kanji) {
      if (!K.second.entries.empty()) {
         if (K.second.index > maxKanji) {
            ::std::set< ::timber::text::unicode::CodePoint> ustr;
            for (auto& j : K.second.entries) {
               getKanjis(j.kanji[0].keb, ustr);
            }
            for (::timber::text::unicode::CodePoint cp : ustr) {
               if (cp != K.second.cp && kanjiIndex[cp] == K.first) {
                  if (kanjiByCp.find(cp) != kanjiByCp.end()) {
                     if (kanji[kanjiByCp[cp]].index != K.first) {
                        allKanjis.push_back(kanji[kanjiByCp[cp]]);
                     }
                  }
               }
            }
         }
         allKanjis.push_back(K.second);
      }
   }
   if (randomize) {

   }

   for (int set = 0; set < maxSets; ++set) {
      int odd = 1;
      auto iterator = allKanjis.begin();
      int nKanjis = 0;
      while (iterator != allKanjis.end() && nKanjis < maxKanjis) {

         KanjiBox options;
         options.strokeWidth = .5;
         options.inset = 1;
         options.font_size = 0;
         options.text_color = 0;

         double left = 0;
         double right = in2pts(PAGE_WIDTH_IN);
         if (odd == 1 || odd == 0) {
            left += in2pts(margin * odd);
            right -= in2pts(margin * (1 - odd));
            odd = 1 - odd;
         }

         KanjiExtent baseExtent = { left, 0, right - left, in2pts(PAGE_HEIGHT_IN) };

         // set the default font
         if (!font.empty()) {
            cairo_select_font_face(ctx, font.c_str(), CAIRO_FONT_SLANT_NORMAL, CAIRO_FONT_WEIGHT_NORMAL);
         }

         cairo_save(ctx);
         bool leftSide = true;

         while (iterator != allKanjis.end()) {

            const Kanji& K = *iterator;
            const KanjiExtent referenceKanji = { baseExtent.x, baseExtent.y, in2pts(0), in2pts(0) };
            KanjiExtent kanjiReadings = { baseExtent.x, baseExtent.y + referenceKanji.h, baseExtent.w / 2, in2pts(0) };

            if (kanjiReadings.y > baseExtent.y + baseExtent.h - in2pts(1)) {
               break;
            }

            if (leftSide) {
               // remain like this
            }
            else {
               kanjiReadings = {baseExtent.x+baseExtent.w/2, baseExtent.y + referenceKanji.h, baseExtent.w/2, in2pts(0)};
            }

            cairo_save(ctx);
            options.font_size = 8;
            KanjiExtent exampleWords = { kanjiReadings.x, kanjiReadings.y + kanjiReadings.h, kanjiReadings.w, in2pts(
                  PAGE_HEIGHT_IN) - (kanjiReadings.y + kanjiReadings.h) };
            exampleWords = drawPracticeWords(ctx, exampleWords, options, K.entries, generator);
            options.font_size = 0;
            cairo_restore(ctx);
            if (exampleWords.h > 0) {
               leftSide = !leftSide;
               if (leftSide) {
                  baseExtent.y = exampleWords.y + exampleWords.h;
                  baseExtent.h = in2pts(PAGE_HEIGHT_IN) - baseExtent.y;
               }
            }
            ++iterator;
            nKanjis++;
         }
         cairo_restore(ctx);

         cairo_surface_mark_dirty(surface);
         cairo_surface_flush(surface);
         cairo_surface_show_page(surface);
      }
   }
   cairo_destroy(ctx);
   cairo_surface_destroy(surface);
   surface = 0;
   return 0;
}
