#include <kanji/parse.h>

#include <iostream>
#include <fstream>
#include <string>
#include <map>

int main(int argc, char * const * argv)
{
   ::std::map< ::std::string,::std::string> xmap;

   xmap["\\t"]="\t";
   xmap["\\n"]="\n";
   xmap["\\\\"]="\\";

   ::std::string osep = "&";
   ::std::string isep = ",";
   bool csv = false;

   int argpos = 1;
   for (argpos = 1; argpos < argc; ++argpos) {
      if (::std::string(argv[argpos]) == "-o") {
         if (argpos + 1 == argc) {
            ::std::cerr << "Missing argument for " << argv[argpos] << ::std::endl;
            return 1;
         }
         osep = argv[++argpos];
      }
      else if (::std::string(argv[argpos]) == "-i") {
         if (argpos + 1 == argc) {
            ::std::cerr << "Missing argument for " << argv[argpos] << ::std::endl;
            return 1;
         }
         isep = argv[++argpos];
      }
      else if (::std::string(argv[argpos]) == "-c") {
         csv = true;
      }
      else {
         break;
      }
   }

   if (xmap.count(isep)==1) {
      isep=xmap[isep];
   }
   if (xmap.count(osep)==1) {
      osep=xmap[osep];
   }

   if (csv && isep != ",") {
      ::std::cerr << "CSV option is incompatible with input delimiter '" << isep << "'" << ::std::endl;
      return 1;
   }

   ::std::ifstream stream(argpos < argc ? argv[argpos++] : "/dev/stdin");
   ::std::vector < std::string > fields;
   int status = 1;
   while (stream && !stream.eof()) {
      if (csv) {
         fields = kanji::readCSVLine(stream);
      }
      else {
         fields = kanji::readDelimitedLine(stream, isep);
      }
      if (!fields.empty()) {
         ::std::string sep;
         for (const ::std::string& f : fields) {
            ::std::cout << sep << f;
            sep = osep;
         }
         ::std::cout << ::std::endl;
         status = 0;
      }
   }

   return status;
}

