#include <kanji/jmdict/Entry.h>
#include <kanji/parse.h>

#include <timber/text/unicode/UnicodeString.h>
#include <timber/text/unicode/UTF8Coding.h>
#include <timber/text/unicode/UCD.h>

#include <cstring>
#include <iostream>
#include <fstream>
#include <string>
#include <map>
#include <cstdlib>
using namespace kanji::jmdict;
using namespace timber::text::unicode;

static bool fixReadings(const Kanji& k, kanji::jmdict::Entry& e)
{
   for (auto i = e.reading.begin(); i != e.reading.end();) {
      const Reading& r = *i;
      bool keepReading = r.re_restr.empty();
      for (const Text& t : r.re_restr) {
         if (t == k.keb) {
            keepReading = true;
            break;
         }
      }
      if (!keepReading) {
         e.reading.erase(i);
      }
      else {
         ++i;
      }
   }
   return !e.reading.empty();
}

static bool processEntry(const std::map< CodePoint, int>& kanjis, kanji::jmdict::Entry& e)
{
   Extension newext;
   newext.field = "kscore";
   newext.type = Extension::NUMBER;
   Extension kcount;
   kcount.field = "kcount";
   kcount.type = Extension::NUMBER;
   Extension kanji;
   kanji.field = "kanji";
   kanji.type = Extension::TEXT;
   UTF8Coding utf8;
   int maxkCount = 0;
   for (auto j = e.kanji.begin(); j != e.kanji.end();) {
      const UnicodeString text(j->keb);
      CodePoint kanjiCP;
      newext.number = 0;
      kcount.number = 0;
      for (size_t i = 0; i < text.length(); ++i) {
         if (::std::strstr(UCD::blockOf(text[i]), "CJK") != nullptr) {
            auto p = kanjis.find(text[i]);
            if (p == kanjis.end()) {
               newext.number = 0;
               break;
            }

            if (p->second > newext.number) {
               kanjiCP = text[i];
               newext.number = p->second;
            }
            ++kcount.number;
         }
      }
      if (newext.number == 0 || maxkCount > kcount.number) {
         e.kanji.erase(j);
         continue;
      }
      // keep track of maximum number of kanji found
      maxkCount = kcount.number;

      // delete any existing value
      for (auto ext = j->ext.begin(); ext != j->ext.end(); ++ext) {
         if (ext->field == newext.field || ext->field == kcount.field || ext->field == kanji.field) {
            j->ext.erase(ext);
            break;
         }
      }
      utf8.encode(&kanjiCP, 1, kanji.text);
      j->ext.push_back(newext);
      j->ext.push_back(kcount);
      j->ext.push_back(kanji);
      ++j;
   }
   for (auto j = e.kanji.begin(); j != e.kanji.end();) {
      for (auto ext = j->ext.begin(); ext != j->ext.end(); ++ext) {
         if (ext->field == kcount.field) {
            if (maxkCount > ext->number) {
               e.kanji.erase(j);
            }
            else {
               ++j;
            }
            break;
         }
      }
   }
   return !e.kanji.empty();
}

int main(int argc, char * const * argv)
{
   std::map< CodePoint, int> scores;

   if (argc < 2) {
      ::std::cerr << "Missing kanji file";
   }
   // load the scores
   {
      ::std::ifstream stream(argv[1]);
      ::std::string line;
      ::std::vector < std::string > fields;
      while (stream && !stream.eof()) {
         fields = kanji::readCSVLine(stream);
         if (fields.size() >= 2 && !fields[0].empty() && !fields[1].empty()) {
            int index = atoi(fields[0].c_str());
            UnicodeString str(fields[1]);
            str = UnicodeString(str[0]);
            if (::std::strstr(UCD::blockOf(str[0]), "CJK") != nullptr) {
               scores[str[0]] = index;
//                  ::std::cerr << index << '\t' << str.utf8() << '\t' << UCD::blockOf(str[0]) << ::std::endl;
            }
            else {
               ::std::cerr << "Not a CJK UNIFIED IDEOGRAPH: " << index << '\t' << str.utf8() << '\t'
                     << UCD::blockOf(str[0]) << ::std::endl;
            }
         }
      }
   }

   try {
      readEntries(argc == 3 ? argv[2] : nullptr, [&] (Entry& e) {
         Extension alternative;
         alternative.field = "alternative";
         alternative.type = Extension::NUMBER;
         alternative.number=0;
         for (const Kanji& k : e.kanji) {
            Entry clone(e);
            clone.kanji.clear();
            clone.kanji.push_back(k);
            if (alternative.number==0) {
               ++alternative.number;
            }
            else {
               clone.kanji[0].ext.push_back(alternative);
            }
            if (!fixReadings(k, clone)) {
               continue;
            }

            // remove any readings, that are not for this kanji

            if (processEntry(scores, clone)) {

               ::std::cout << clone;
            }
         }
      });
   }
   catch (...) {
      ::std::cerr << "Failed to process entries" << ::std::endl;
      return 1;
   }
   return 0;
}
