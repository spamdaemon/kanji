#include <kanji/jmdict/Entry.h>

#include <iostream>
#include <fstream>
#include <string>
#include <memory>
#include <set>

using namespace kanji::jmdict;

static bool processEntry(const std::set< Text>& pri, kanji::jmdict::Entry& e)
{
   if (!e.kanji.empty()) {
      for (auto j = e.kanji.begin(); j != e.kanji.end();) {
         Kanji& k = *j;
         for (auto i = k.ke_pri.begin(); i != k.ke_pri.end();) {
            if (pri.count(*i) > 0) {
               ++i;
            }
            else {
               k.ke_pri.erase(i);
            }
         }
         if (k.ke_pri.empty()) {
            e.kanji.erase(j);
         }
         else {
            ++j;
         }
      }
      if (e.kanji.empty()) {
         return false;
      }
   }

   for (auto j = e.reading.begin(); j != e.reading.end();) {
      Reading& k = *j;
      bool eraseReading = k.re_pri.empty() && e.kanji.empty();
      for (auto i = k.re_pri.begin(); i != k.re_pri.end();) {
         if (pri.count(*i) > 0) {
            eraseReading=false;
            ++i;
         }
         else {
            k.re_pri.erase(i);
         }
      }
      // we only want to erase a reading, if there is no kanji for that
      // reading; otherwise, we need to keep it.
      if (eraseReading) {
         e.reading.erase(j);
      }
      else {
         ++j;
      }
   }
   if (e.reading.empty()) {
      return false;
   }
   return true;
}

int main(int argc, char * const * argv)
{
   std::set< Text> pri;
   for (int i = 1; i < argc; ++i) {
      pri.insert(argv[i]);
   }
   try {
      readEntries(nullptr, [&] (Entry& e) {
         if (processEntry(pri,e)) {
            ::std::cout << e;
         }
      });
   }
   catch (...) {
      ::std::cerr << "Failed to process entries" << ::std::endl;
      return 1;
   }
   return 0;
}
