#include <kanji/jmdict/Entry.h>
#include <iostream>

using namespace kanji::jmdict;

static int getScore(const Kanji& k)
{
   for (const Extension& ext : k.ext) {
      if (ext.field == "kscore") {
         return ext.number;
      }
   }
   return 0;
}

static const ::std::string getKanji(const Kanji& k)
{
   for (const Extension& ext : k.ext) {
      if (ext.field == "kanji") {
         return ext.text;
      }
   }
   return "";
}

static void processEntry(Entry& e)
{
   bool nl = false;
   for (const Kanji& k : e.kanji) {
      if (!k.keb.empty()) {
         if (!nl) {
            ::std::cout << getScore(k) << '\t' << getKanji(k) << '\t';
         }
         if (nl) {
            ::std::cout << '|';
         }
         ::std::cout << k.keb;
         nl = true;
      }
   }
   if (nl) {
      char sep = '\t';
      for (const Reading& r : e.reading) {
         if (!r.reb.empty()) {
            ::std::cout << sep << r.reb;
            sep = '|';
         }
      }
   }
   if (nl) {
      for (const Sense& s : e.sense) {
         char sep = '\t';
         for (const Gloss& g : s.gloss) {
            ::std::cout << sep << g.gloss;
            sep = '|';
         }
      }
   }
   if (nl) {
      ::std::cout << std::endl;
   }
}

int main(int argc, char * const * argv)
{
   try {
      readEntries(argc == 2 ? argv[1] : nullptr, processEntry);
   }
   catch (...) {
      ::std::cerr << "Failed to process entries" << ::std::endl;
      return 1;
   }
   return 0;
}
