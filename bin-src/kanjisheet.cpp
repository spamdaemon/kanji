#include <kanji/jmdict/Entry.h>
#include <timber/text/unicode/UnicodeString.h>
#include <timber/text/unicode/UCD.h>
#include <iostream>
#include <fstream>
#include <set>
#include <map>

#include <cairo/cairo.h>
#include <cairo/cairo-pdf.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

using namespace std;

static const double PAGE_WIDTH_IN = 8;
static const double PAGE_HEIGHT_IN = 10.5;

typedef struct
{
      double x, y;
      double w, h;
} KanjiExtent;

typedef struct
{
      double inset;
      double strokeWidth;
      double text_color;
} KanjiBox;

static double in2pts(double inches)
{
   return floor(16384 * (inches * 72)) / 16384.0;
}

static void adjustExtent(KanjiExtent* extent, KanjiBox options)
{
   double inset = options.inset + 0;//options.strokeWidth;
   extent->x += inset;
   extent->y += inset;
   extent->w -= 2 * inset;
   extent->h -= 2 * inset;
}

static KanjiExtent drawCharBoxRows(cairo_t* c, KanjiExtent extent, KanjiBox options, double sz)
{
   KanjiExtent ext;
   ext.h = sz;
   ext.w = sz;

   KanjiExtent actual = extent;
   adjustExtent(&extent, options);

   double w = ext.w * ((int) ((extent.w) / ext.w));
   double h = ext.h * ((int) ((extent.h)) / ext.h);

   double minx = extent.x;
   double miny = extent.y;
   double maxx = extent.x + w;
   double maxy = extent.y + h;

   maxx=maxy=0;
   for (ext.y = extent.y; ext.y < extent.y + h + ext.h/2; ext.y += ext.h) {
      maxy=ext.y;
   }
   for (ext.x = extent.x; ext.x < extent.x + w + ext.w/2; ext.x += ext.w) {
      maxx=ext.x;
   }
   cairo_set_line_width(c, options.strokeWidth);
   cairo_set_source_rgb(c, 0, 0, 0);

   // draw horizontal lines
   for (ext.y = extent.y; ext.y < maxy + ext.h/2; ext.y += ext.h) {
      cairo_move_to(c, minx, ext.y);
      cairo_line_to(c, maxx, ext.y); // horizontal
      cairo_close_path(c);
      cairo_stroke(c);
   }

   for (ext.x = extent.x; ext.x < maxx + ext.w/2; ext.x += ext.w) {
      cairo_move_to(c, ext.x, miny);
      cairo_line_to(c, ext.x, maxy); // horizontal
      cairo_close_path(c);
      cairo_stroke(c);
   }

   return actual;
}

int main(int argc, char** argv)
{
#if CAIRO_HAS_PDF_SURFACE != 1
#error "PDF surface not available\n";
#endif

   ::std::string output;
   double margin = in2pts(.0);
   double size = in2pts(.4);
   double evenOddMargin = in2pts(.25);
   size_t nPages = 1;

   for (int i = 1; i < argc; ++i) {
      if (strcmp("-o", argv[i]) == 0) {
         output = argv[++i];
      }
      else if (strcmp("-n", argv[i]) == 0) {
         nPages = atoi(argv[++i]);
      }
      else if (strcmp("-s", argv[i]) == 0) {
         size = in2pts(atof(argv[++i]));
      }
      else if (strcmp("-m", argv[i]) == 0) {
         margin = in2pts(atof(argv[++i]));
      }
      else if (strcmp("-M", argv[i]) == 0) {
         evenOddMargin = in2pts(atof(argv[++i]));
      }
   }
   if (margin < 0 || (margin == 0 && margin != 0)) {
      margin = 0;
   }
   if (evenOddMargin < 0 || (evenOddMargin == 0 && evenOddMargin != 0)) {
      evenOddMargin = 0;
   }

   if (output.empty()) {
      output = "/dev/stdout";
   }

   cairo_surface_t* surface = cairo_pdf_surface_create(output.c_str(), in2pts(PAGE_WIDTH_IN), in2pts(PAGE_HEIGHT_IN));
   if (surface == 0) {
      fprintf(stderr, "Failed to create pdf surface\n");
      return 1;
   }

   cairo_t* ctx = cairo_create(surface);

   int odd = 1;
   for (size_t i = 0; i < nPages; ++i) {
      KanjiBox options;
      options.strokeWidth = .5;
      options.inset = 0;
      options.text_color = 0;

      double left = margin;
      double right = in2pts(PAGE_WIDTH_IN) - margin;
      if (odd == 1 || odd == 0) {
         left += evenOddMargin * odd;
         right -= evenOddMargin * (1 - odd);
         odd = 1 - odd;
      }

      double w = size * ((int) ((right - left) / size));
      double h = size * ((int) ((in2pts(PAGE_HEIGHT_IN)) / size));

      KanjiExtent baseExtent = { left, 0, w, h };

      cairo_save(ctx);
      options.text_color = .95;
      drawCharBoxRows(ctx, baseExtent, options, size);
      options.text_color = 0;
      cairo_restore(ctx);

      cairo_surface_mark_dirty(surface);
      cairo_surface_flush(surface);
      cairo_surface_show_page(surface);
   }
   cairo_destroy(ctx);
   cairo_surface_destroy(surface);
   surface = 0;
   return 0;
}
