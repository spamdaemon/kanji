#include <kanji/jmdict/Entry.h>
#include <kanji/jmdict/Sense.h>
#include <kanji/jmdict/PartOfSpeech.h>
#include <kanji/conjugate.h>
#include <kanji/parse.h>
#include <timber/text/unicode/UnicodeString.h>
#include <timber/text/unicode/UCD.h>
#include <iostream>
#include <fstream>
#include <set>
#include <map>
#include <cassert>

#include <stdlib.h>
#include <string.h>

using namespace std;

struct Kanji
{
      int index;
      ::std::string kanji;
      ::timber::text::unicode::CodePoint cp;
      ::std::string readings;
      vector< kanji::jmdict::Entry> entries;
};

static bool isValidSense(const kanji::jmdict::Entry& E, const kanji::jmdict::Sense& sense)
{
   if (kanji::jmdict::typeOf(sense) != kanji::jmdict::SenseType::VERB) {
      return false;
   }
   if (!sense.lsource.empty()) {
      return false;
   }
   for (auto x : sense.stagk) {
      if (x != E.kanji[0].keb) {
         return false;
      }
   }
   for (auto x : sense.misc) {
      if (x == "archaism") {
         return false;
      }
      if (x == "obscure term") {
         return false;
      }
      if (x == "obsolete term") {
         return false;
      }
      if (x == "abbreviation") {
         return false;
      }
   }
   for (auto pos : sense.pos) {
      if (!::kanji::jmdict::isProperWord(pos)) {
         return false;
      }
   }
   return true;
}

static bool isValidEntry(const kanji::jmdict::Entry& E, size_t nSenses)
{
   for (const kanji::jmdict::Extension& e : E.kanji[0].ext) {
      if (e.field == "alternative") {
         return false;
      }
   }
   for (size_t i = 0; i < E.sense.size() && i < nSenses; ++i) {
      if (isValidSense(E, E.sense[i])) {
         return true;
      }
   }
   return false;
}
static bool removeInvalidSenses(kanji::jmdict::Entry& E)
{
   for (auto i = E.sense.begin(); i != E.sense.end();) {
      if (isValidSense(E, *i)) {
         ++i;
      }
      else {
         E.sense.erase(i);
      }
   }
   return !E.sense.empty();
}

static void getKanjis(const ::std::string& text, ::std::set< ::timber::text::unicode::CodePoint>& res)
{
   ::timber::text::unicode::UnicodeString str(text);
   for (size_t i = 0; i < str.length(); ++i) {
      if (::std::strstr(::timber::text::unicode::UCD::blockOf(str[i]), "CJK") != nullptr) {
         res.insert(str[i]);
      }
   }
}

static int getScore(const kanji::jmdict::Entry& e)
{
   for (auto& k : e.kanji) {
      for (auto& ext : k.ext) {
         if (ext.field == "kscore") {
            return ext.number;
         }
      }
   }
   return 0;
}

static ::std::string getPriority(::std::string p)
{
   if (p.find("core2k") == 0) {
      p = ::std::string("0") + p;
   }
   else if (p.find("core4k") == 0) {
      p = ::std::string("1") + p;
   }
   else if (p.find("core6k") == 0) {
      p = ::std::string("e") + p;
   }
   else if (p.find("tobira") == 0) {
      p = ::std::string("00") + p;
   }
   else if (p.find("core10k") == 0) {
      p = ::std::string("f") + p;
   }
   else if (p.find("ichi") == 0) {
      p = ::std::string("b") + p;
   }
   else if (p.find("news") == 0) {
      p = ::std::string("c") + p;
   }
   else if (p.find("spec") == 0) {
      p = ::std::string("d") + p;
   }
   else if (p.find("nf") == 0) {
      p = ::std::string("a") + p;
   }
   else {
      p = "";
   }
   return p;
}

static ::std::string getPriority(const kanji::jmdict::Entry& e, const ::std::string& maxPri)
{
   ::std::string pri;
   for (auto& k : e.kanji) {
      for (::std::string p : k.ke_pri) {
         p = getPriority(p);
         if (p.empty() || p > maxPri) {
            continue;
         }
         if (pri.empty() || p < pri) {
            pri = ::std::move(p);
         }
      }
   }
   return pri;
}

static ::std::map< int, Kanji> createKanji(const ::std::string& file, size_t maxKanjiIndex)
{
   ::std::map< int, Kanji> result;
   ::std::ifstream stream(file);
   ::std::string line;
   ::std::vector < std::string > fields;
   while (stream && !stream.eof()) {
      fields = kanji::readCSVLine(stream);
      if (fields.size() >= 2 && !fields[0].empty() && !fields[1].empty()) {
         Kanji kanji;
         kanji.index = atoi(fields[0].c_str());
         if (maxKanjiIndex > 0 && kanji.index > maxKanjiIndex) {
            continue;
         }
         kanji.kanji = fields[1];
         kanji.readings = fields[2];
         ::timber::text::unicode::UnicodeString str(fields[1]);
         str = ::timber::text::unicode::UnicodeString(str[0]);
         if (::std::strstr(::timber::text::unicode::UCD::blockOf(str[0]), "CJK") != nullptr) {
            kanji.cp = str[0];
            result[kanji.index] = kanji;
         }
      }
   }
   return ::std::move(result);
}

int main(int argc, char** argv)
{
   ::std::string output;
   ::std::string kanjiFile("data/kanji.csv");
   bool originalOrder = false;
   size_t nSenses = 3;
   size_t nEntries = 10;
   string delim = "\t";
   size_t maxKanjiIndex = 0;

   for (int i = 1; i < argc; ++i) {

      if (strcmp("-O", argv[i]) == 0) {
         originalOrder = true;
      }
      else if (strcmp("-s", argv[i]) == 0) {
         nSenses = atoi(argv[++i]);
      }
      else if (strcmp("-e", argv[i]) == 0) {
         nEntries = atoi(argv[++i]);
      }
      else if (strcmp("-o", argv[i]) == 0) {
         output = argv[++i];
      }
      else if (strcmp("-N", argv[i]) == 0) {
         maxKanjiIndex = atoi(argv[++i]);
      }
      else if (strcmp("-k", argv[i]) == 0) {
         kanjiFile = argv[++i];
      }
   }
   if (output.empty()) {
      output = "/dev/stdout";
   }

   // read all the kanji
   map< int, Kanji> kanji = createKanji(kanjiFile, maxKanjiIndex);
   map< ::timber::text::unicode::CodePoint, int> kanjiByCp;
   for (auto& x : kanji) {
      kanjiByCp[x.second.cp] = x.first;

   }
   const ::std::string maxPri = getPriority("nf");

   kanji::jmdict::readEntries(::std::cin, [&kanji,&maxPri] (kanji::jmdict::Entry& e) {
      int s = getScore(e);
      auto i = kanji.find(s);
      if (i!=kanji.end()) {
         auto pri = getPriority(e,maxPri);
         if (pri.empty()) {
            return;
         }

         for (auto j=i->second.entries.begin();j!=i->second.entries.end();++j) {
            if (!j->kanji[0].keb.empty()) {
               if (pri < getPriority(*j,maxPri)) {
                  i->second.entries.insert(j,::std::move(e));
                  pri.clear();
                  break;
               }
            }
         }
         if (!pri.empty()) {
            i->second.entries.push_back(::std::move(e));
         }
         if (i->second.entries.size() > 3) {
//            i->second.entries.pop_back();
      }
   }
});

   map< ::timber::text::unicode::CodePoint, int> kanjiIndex;
   for (auto& i : kanji) {
      ::std::set< ::timber::text::unicode::CodePoint> ustr;
      for (const kanji::jmdict::Entry& e : i.second.entries) {
         getKanjis(e.kanji[0].keb, ustr);
      }
      for (::timber::text::unicode::CodePoint cp : ustr) {
         auto k = kanjiIndex.find(cp);
         if (k == kanjiIndex.end() || i.first < k->second) {
            kanjiIndex[cp] = i.first;
         }
      }
   }

   vector< Kanji> allKanjis;
   for (auto K : kanji) {
      if (originalOrder) {
         allKanjis.push_back(K.second);
      }
      else if (!K.second.entries.empty()) {
         ::std::set< ::timber::text::unicode::CodePoint> ustr;
         for (auto& j : K.second.entries) {
            getKanjis(j.kanji[0].keb, ustr);
         }
         for (::timber::text::unicode::CodePoint cp : ustr) {
            if (cp != K.second.cp && kanjiIndex[cp] == K.first) {
               if (kanjiByCp.find(cp) != kanjiByCp.end()) {
                  if (kanji[kanjiByCp[cp]].index != K.first) {
                     allKanjis.push_back(kanji[kanjiByCp[cp]]);
                  }
               }
            }
         }
         allKanjis.push_back(K.second);
      }
   }

   ::std::ofstream stream(output);
   stream << "#";
   stream << '"' << "index" << '"' << delim;
   stream << '"' << "keb" << '"' << delim;
   stream << '"' << "reb" << '"' << delim;
   stream << '"' << "audio" << '"' << delim;
   stream << '"' << "sense" << '"' << delim;
   stream << '"' << "transitive" << '"' << delim;
   stream << '"' << "stem" << '"' << delim;
   stream << '"' << "type" << '"' << delim;
   stream << '"' << "inf.nonpast" << '"' << delim;
   stream << '"' << "inf.neg.nonpast" << '"' << delim;
   stream << '"' << "inf.past" << '"' << delim;
   stream << '"' << "inf.neg.past" << '"' << delim;
   stream << '"' << "formal.nonpast" << '"' << delim;
   stream << '"' << "formal.neg.nonpast" << '"' << delim;
   stream << '"' << "te-form" << '"' << delim;
   stream << '"' << "te-form.neg" << '"' << delim;
   stream << '"' << "volitional" << '"' << delim;
   stream << '"' << "formal.volitional" << '"' << delim;
   stream << '"' << "potential" << '"' << delim;
   stream << '"' << "conditional" << '"' << delim;
   stream << '"' << "passive" << '"' << delim;
   stream << '"' << "causative" << '"' << delim;
   stream << '"' << "imperative" << '"' << delim;
   stream << ::std::endl;

   char fill = stream.fill('0');
   int width = stream.width(4);
   size_t index = 0;
   for (const Kanji& K : allKanjis) {

      // EMIT the practice words
      size_t e = 0;
      for (const kanji::jmdict::Entry& E : K.entries) {
         if (e == nEntries) {
            break;
         }
         ++e;
         // we need to filter out invalid sense and replace them
         // with blanks. This is so that we can update the flash cards
         // otherwise, we'll re-associate cards with new values
         if (isValidEntry(E, nSenses)) {
            string audio;
            for (const kanji::jmdict::Extension& ext : E.reading[0].ext) {
               if (ext.type == kanji::jmdict::Extension::TEXT && ext.field == "audio") {
                  audio = ext.text;
                  break;
               }
            }
            for (size_t i = 0; i < E.sense.size() && i < nSenses; ++i) {
               if (isValidSense(E, E.sense[i])) {
                  char vt = kanji::jmdict::verbType(E.sense[i]);
                  if (vt != '-') {

                     stream.fill('0');
                     stream.width(4);
                     stream << index++ << delim;
                     stream.fill(fill);
                     stream.width(width);
                     stream << '"' << E.kanji[0].keb << '"' << delim;
                     stream << '"' << E.reading[0].reb << '"' << delim;
                     stream << audio << delim;
                     stream << '"' << E.sense[i].gloss[0].gloss << '"' << delim;
                     if (kanji::jmdict::isTransitiveVerb(E.sense[i])) {
                        stream << "\"vt\"" << delim;
                     }
                     else {
                        stream << delim;
                     }
                     for (auto x : kanji::conjugateVerb(E.kanji[0].keb, vt == 'r')) {
                        stream << '"' << x << '"' << delim;
                     }
                     stream << getPriority(E, maxPri);
                     stream << ::std::endl;
                     break;
                  }
               }
            }
         }
      }
   }
   return 0;
}
