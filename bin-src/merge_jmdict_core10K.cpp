#include <kanji/jmdict/Entry.h>
#include <kanji/parse.h>

#include <iostream>
#include <sstream>
#include <fstream>
#include <string>
#include <memory>
#include <vector>
#include <map>

using namespace kanji::jmdict;

static int parseLong(const ::std::string& str)
{
   ::std::istringstream s(str);
   int res;
   s >> res;
   if (!s) {
      return -1;
   }
   return res;
}

static bool processEntry(const ::std::map< ::std::string, ::std::vector< ::std::string> >& core10K,
      const ::std::map< ::std::string, ::std::vector< ::std::string> >& core10Kreadings, kanji::jmdict::Entry& e)
{
   Extension audio;
   audio.field = "audio";
   audio.type = Extension::TEXT;
   Extension ext10K;
   ext10K.field = "core10k";
   ext10K.type = Extension::NUMBER;
   Extension ext2K;
   ext2K.field = "core2k";
   ext2K.type = Extension::NUMBER;
   Extension ext4K;
   ext4K.field = "core4k";
   ext4K.type = Extension::NUMBER;
   Extension ext6K;
   ext6K.field = "core6k";
   ext6K.type = Extension::NUMBER;
   Extension ext8K;
   ext8K.field = "core8k";
   ext8K.type = Extension::NUMBER;

   for (auto j = e.kanji.begin(); j != e.kanji.end(); ++j) {
      Kanji& k = *j;

      auto i = core10K.find(k.keb);
      if (i != core10K.end()) {

         ext10K.number = parseLong(i->second[1]);
         if (ext10K.number >= 0) {
            k.ext.push_back(ext10K);
            k.ke_pri.push_back(ext10K.field);
            if (ext10K.number < 8001) {
               ext8K.number = ext10K.number;
               k.ext.push_back(ext8K);
               k.ke_pri.push_back(ext8K.field);
            }
            if (ext10K.number < 6001) {
               ext6K.number = ext10K.number;
               k.ext.push_back(ext6K);
               k.ke_pri.push_back(ext6K.field);
            }
            if (ext10K.number < 4001) {
               ext4K.number = ext10K.number;
               k.ext.push_back(ext4K);
               k.ke_pri.push_back(ext4K.field);
            }
            if (ext10K.number < 2001) {
               ext2K.number = ext10K.number;
               k.ext.push_back(ext2K);
               k.ke_pri.push_back(ext2K.field);
            }
         }
      }
   }
   for (auto j = e.reading.begin(); j != e.reading.end(); ++j) {
      Reading& k = *j;
      auto i = core10Kreadings.find(k.reb);
      if (i != core10Kreadings.end() && !i->second[10].empty()) {
         audio.text = i->second[10];
         k.ext.push_back(audio);
      }
   }

   for (auto j = e.reading.begin(); j != e.reading.end(); ++j) {
      Reading& k = *j;
      auto i = core10K.find(k.reb);

      if (i != core10K.end()) {
         ext10K.number = parseLong(i->second[1]);
         if (ext10K.number >= 0) {
            k.ext.push_back(ext10K);
            k.re_pri.push_back(ext10K.field);
            if (ext10K.number < 8001) {
               ext8K.number = ext10K.number;
               k.ext.push_back(ext8K);
               k.re_pri.push_back(ext8K.field);
            }
            if (ext10K.number < 6001) {
               ext6K.number = ext10K.number;
               k.ext.push_back(ext6K);
               k.re_pri.push_back(ext6K.field);
            }
            if (ext10K.number < 4001) {
               ext4K.number = ext10K.number;
               k.ext.push_back(ext4K);
               k.re_pri.push_back(ext4K.field);
            }
            if (ext10K.number < 2001) {
               ext2K.number = ext10K.number;
               k.ext.push_back(ext2K);
               k.re_pri.push_back(ext2K.field);
            }
         }
      }
   }
   return true;
}

int main(int argc, char * const * argv)
{
// read the core 10K file
   if (argc < 2) {
      ::std::cerr << "Missing core 10K file" << ::std::endl;
      return 1;
   }

   ::std::map< ::std::string, ::std::vector< ::std::string> > core10K;
   ::std::map< ::std::string, ::std::vector< ::std::string> > core10Kreadings;
   ::std::ifstream stream(argv[1]);
   while (stream && !stream.eof()) {
      ::std::vector < ::std::string > fields = kanji::readDelimitedLine(stream, "\t");
      if (fields.size() > 3 && !fields[2].empty()) {
         core10K[fields[2]] = fields;
      }
      if (fields.size() > 4 && !fields[3].empty()) {
         // also, index by kana only
         core10Kreadings[fields[3]] = fields;
      }
   }
   if (core10K.empty()) {
      ::std::cerr << "No core 10K file read" << ::std::endl;
      return 1;
   }
   try {
      readEntries(nullptr, [&] (Entry& e) {
         if (processEntry(core10K,core10Kreadings,e)) {
            ::std::cout << e;
         }
      });
   }
   catch (...) {
      ::std::cerr << "Failed to process entries" << ::std::endl;
      return 1;
   }
   return 0;
}
