#include <kanji/jmdict/Entry.h>

#include <iostream>
#include <fstream>
#include <string>
#include <memory>

using namespace kanji::jmdict;

int main(int argc, char * const * argv)
{
   ::std::unique_ptr < ::std::ifstream > file;
   ::std::istream* stream = nullptr;
   if (argc != 2) {
      ::std::cerr << "No file specified; reading from stdin" << ::std::endl;
      stream = &::std::cin;
   }
   else {
      file.reset(new ::std::ifstream(argv[1]));
      stream = file.get();
   }
   try {
      size_t n = 0;
      while (*stream) {
         auto e = readRawEntry(*stream);
         if (e.n == 0) {
            break;
         }
         else {
            ++n;
         }
      }
      ::std::cout << n << ::std::endl;
   }
   catch (...) {
      ::std::cerr << "Failed to process entries" << ::std::endl;
      return 1;
   }
   return 0;
}
