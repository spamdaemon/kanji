#!/bin/bash

SCRIPT="$(readlink -e "$0")";
SCRIPT_DIR="$(dirname "$SCRIPT")";

BIN_DIR="$(readlink -e "${SCRIPT_DIR}/../bin")";
DATA_DIR="$(readlink -e "${SCRIPT_DIR}/../data")";

VERBOSE=false;

gen_seed()
{
    date '+%s' 2>/dev/null;
}

new_seed()
{
    local old="$1";
    local seed="$(gen_seed)";
    while [ "$seed" == "$old" ]; do
	sleep .1;
	seed="$(gen_seed)";
    done;
    echo "$seed";
}

message()
{ echo "$@" 1>&2; }

run()
{
    if $VERBOSE; then
	"$@";
    else
	"$@" 2>/dev/null;
    fi;
}

usage()
{
    local status="${1:-1}";
    message "$0 [-h] [-v] [-s <seed>] [-M <number>] [-S <number>] [-K <number>] [-p <output-file>] [-l <output-file>]";
    message " Generate either pdf from JMDict and Core2K";
    message " If no output file is specified, then the stdout is used";
    message "Options:"
    message "  -h    this message";
    message "  -p    generate kanji practice sheet";
    message "  -l    generate kanji learning sheet";
    message "  -M    the highest numbered kanji you already know";
    message "  -K    the number of kanji to output in the practice sheet";
    message "  -S    the number of sets of kanjis to output in the practice sheet";
    message "  -s    random seed (default: now)";
    message "  -R    randomize practice sheets";
    message "  -v    produce verbose or debugging output on stderr";
    message "The -p argument may be repeated to generate multiple practice sheets. Implies seed=now";

    exit $status;
}

SEED=;
LEARN=;
PRACTICE=;
MAX_KANJI=0;
RANDOMIZE=
declare -a PRACTICE_OPTS;

OPTIND=1;
while getopts "hvl:p:s:S:K:M:" opt; do
    case "$opt" in
	l)
	    LEARN="${OPTARG}";;
	p)
	    PRACTICE="${OPTARG}";;
	R)
	    RANDOMIZE="-R";;
	M)
	    MAX_KANJI="${OPTARG}";;
	S)
	    PRACTICE_OPTS+=("-S" "$OPTARG");;
	K)
	    PRACTICE_OPTS+=("-K" "$OPTARG");;
	v)
	    VERBOSE=true;;
	s)
	    SEED="${OPTARG}";;
	h)
	    usage 0;;
	*)
	    usage;;
    esac;
done;
shift $((OPTIND-1));


TDIR=
cleanup()
{
    if [ -n "${TDIR}" ]; then
	rm -rf "${TDIR}";
    fi;
}
trap cleanup EXIT;

TDIR="$(mktemp -d)";
if [ -z "$TDIR" ]; then
    message "Failed to create a temporary direction";
    exit 1;
fi;

run "${BIN_DIR}/parse_jmdict" < "${DATA_DIR}/JMdict_e.xml"  > "${TDIR}/jmdict.bin";
if [ $? -ne 0 ]; then
    message "Failed to convert JMDict XML into binary";
    exit 1;
fi;

run "${BIN_DIR}/merge_jmdict_core10K" "${DATA_DIR}/Core10K.txt" < "${TDIR}/jmdict.bin" > "${TDIR}/jmdict_core10K.bin";
if [ $? -ne 0 ]; then
    message "Failed to merge with core10k";
    exit 1;
fi;

run "${BIN_DIR}/merge_jmdict_tobira" "${DATA_DIR}/Tobira.txt" < "${TDIR}/jmdict_core10K.bin" > "${TDIR}/jmdict_core10K_tobira.bin";
if [ $? -ne 0 ]; then
    message "Failed to merge with core10k";
    exit 1;
fi;

run "${BIN_DIR}/filter_jmdict" tobira core2k < "${TDIR}/jmdict_core10K_tobira.bin" > "${TDIR}/core2k_tobira.bin";
if [ $? -ne 0 ]; then
    message "Failed to merge with filter core2K/tobira";
    exit 1;
fi;


run "${BIN_DIR}/score_jmdict" "${DATA_DIR}/kanji.csv" < "${TDIR}/core2k_tobira.bin" > "${TDIR}/core2k_tobira.scored";
if [ $? -ne 0 ]; then
    message "Failed to score core2k.bin with kanji.csv";
    exit 1;
fi;

if [ -n "$LEARN" ]; then
    run "${BIN_DIR}/kanji2pdf" -k "${DATA_DIR}/kanji.csv" < "${TDIR}/core2k_tobira.scored"  > "${TDIR}/x.pdf";
    if [ $? -ne 0 ]; then
	message "Failed to create pdf for learning";
	exit 1;
    fi;
    mv -f "${TDIR}/x.pdf" "$LEARN";
    if [ $? -ne 0 ]; then
	message "Failed to create $LEARN pdf";
	exit 1;
    fi;
	
fi;

OLD_SEED=now;
if [ -z "$SEED" ]; then
    SEED=now;
fi;
if [ -n "$PRACTICE" ]; then
    if [ "$SEED" == "now" ]; then
	SEED="$(new_seed "$OLD_SEED")";
    fi;
    
    run "${BIN_DIR}/practice2pdf" $RANDOMIZE -M $MAX_KANJI "${PRACTICE_OPTS[@]}"  -s "${SEED}" -k "${DATA_DIR}/kanji.csv" < "${TDIR}/core2k_tobira.scored"  > "${TDIR}/x.pdf";
    if [ $? -ne 0 ]; then
	message "Failed to create pdf $PRACTICE";
	exit 1;
    fi;
    mv -f "${TDIR}/x.pdf" "$PRACTICE";
    if [ $? -ne 0 ]; then
	message "Failed to create $P pdf";
	exit 1;
    fi;
fi;


